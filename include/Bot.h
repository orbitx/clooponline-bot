#pragma once

#include <Router.h>
#include <cxxtools/arg.h>
#include <cxxtools/remoteprocedure.h>
#include <cxxtools/json/rpcclient.h>
#include <cxxtools/json/httpclient.h>
#include <subscribtion_types.h>
#include <packet_types.h>

#define THROW_ERROR_CODE(ERROR_ENUM_NAME) \
      throw ThriftException(ExceptionCode::ERROR_ENUM_NAME);

#define HANDLE_METHOD_HEAD(METHOD) \
      auto treqPacket = (ThriftParser*)reqPacket; \
      auto trepPacket = (ThriftParser*)repPacket; \
      if (! treqPacket->__isset.p##METHOD) \
      { \
         THROW_ERROR_CODE(INVALID_PACKET) \
      } \
      METHOD& innerpack = treqPacket->p##METHOD; \

enum ConnectionMode
{
   CONNECTION_TCP_WEBSOCKET = 1,
   CONNECTION_TCP_SOCKET
};

class Bot : public Router
{
   struct Response
   {
      std::string result;
      std::string reason;
   };

   struct GetIDResponse : public Response
   {
      uint64_t    uid;
      std::string pass;
   };

   struct LoginResponse : public Response
   {
      std::string OTP;
   };

   struct RocoverResponse : public Response
   {
      uint64_t    uid;
      std::string pass;
   };

   struct UserInfo
   {
      uint64_t    uid;
      std::string nickname;
      std::string avatar;
   };

   struct UserInfoResponse : public Response
   {
      std::vector<UserInfo> mInfos;
   };

   struct RPCFunctions
   {
      cxxtools::RemoteProcedure<GetIDResponse>* generateAccount;
      cxxtools::RemoteProcedure<LoginResponse, uint64_t, std::string, std::string>* login;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string>* updateNickname;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string>* updateEmail;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string>* updateAvatar;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string>* updatePassword;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string, std::string, std::string, std::string>* updateUser;
      cxxtools::RemoteProcedure<Response, std::string>* recover;
      cxxtools::RemoteProcedure<RocoverResponse, std::string, std::string>* recoverWithToken;
      cxxtools::RemoteProcedure<UserInfoResponse, std::vector<uint64_t>>* getUserProfiles;
      cxxtools::RemoteProcedure<UserInfoResponse, std::string, std::string>* getUserProfileFromEmail;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string, float>* updateProgress;
      cxxtools::RemoteProcedure<Response, uint64_t, uint64_t, std::string>* achievementUnlocked;

      RPCFunctions()
      {
         generateAccount = nullptr;
         login = nullptr;
         updateNickname = nullptr;
         updateEmail = nullptr;
         updateAvatar = nullptr;
         updatePassword = nullptr;
         updateUser = nullptr;
         recover = nullptr;
         recoverWithToken = nullptr;
         getUserProfiles = nullptr;
         getUserProfileFromEmail = nullptr;
         updateProgress = nullptr;
         achievementUnlocked = nullptr;
      }

      ~RPCFunctions()
      {
         if (generateAccount)
            delete generateAccount;
         if (login)
            delete login;
         if (updateNickname)
            delete updateNickname;
         if (updateEmail)
            delete updateEmail;
         if (updateAvatar)
            delete updateAvatar;
         if (updatePassword)
            delete updatePassword;
         if (updateUser)
            delete updateUser;
         if (recover)
            delete recover;
         if (recoverWithToken)
            delete recoverWithToken;
         if (getUserProfiles)
            delete getUserProfiles;
         if (getUserProfileFromEmail)
            delete getUserProfileFromEmail;
         if (updateProgress)
            delete updateProgress;
         if (achievementUnlocked)
            delete achievementUnlocked;
      }
   };

   friend void operator>>= (const cxxtools::SerializationInfo& si, Response& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, GetIDResponse& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, LoginResponse& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, RocoverResponse& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, UserInfo& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, UserInfoResponse& re);

public:
   Bot();
   ~Bot();

   virtual void initialize();
   virtual void initialize(std::string configFileAddr, std::string targetObj = "");
   virtual void login();
   virtual void connectToGameServer();
   virtual void disconnect();
   virtual void reconnect();
   virtual void sendSubs(float indicator = 0, uint poolTime = 0, float maxIndicatorDistance = 0);
   virtual void sendUSubs();
   virtual void sendTempLeave();
   virtual void sendReady();
   virtual void sendRPC(uint64_t sendAs = 0, bool cached = false);
   virtual void sendRPC(std::vector<uint64_t> receptors, uint64_t sendAs = 0, bool cached = false);
   virtual void sendHandleActiveSession(uint sid, std::string action, uint pindex = 0, uint rpcindex = 0);
   virtual void sendProgress(std::string field, float progressDiff);
   virtual void sendAchievement(uint64_t achieved);
   virtual void sendStartSession();
   virtual void sendCancelStartSession();
   virtual void sendCreateSession();
   virtual void sendJoinSession();
   virtual void sendSessionList();
   virtual void sendSetProperty(std::string operation, std::string path, int val, int cachedVal);
   virtual void sendSetProperty(std::string operation, std::string path, std::string val);
   virtual void sendSetProperty(std::string operation, std::string path, std::vector<int> val);
   virtual void sendSetProperty(std::string operation, std::string path, std::map<std::string, int> val);
   virtual void sendGetProperty();
   virtual void sendChangeSide(uint newside, uint64_t asUID = 0);
   virtual void sendLeave();
   virtual void sendKick(uint64_t uid);
   virtual void sendTerminate();
   virtual void sendGetActiveSessions();
   virtual void sendChangeConfig(clooponline::packet::SessionConfigs conf);
   //TODO: seems like an interesting idea but there is race conditions!
   void waitForPacket(clooponline::packet::PacketType::type packet);

   uint                 mSessionID;
   ProfilePtr           mServer;
   std::string          mServerIP;
   uint16_t             mGameServerPort;
   uint16_t             mAAAServerPort;
   uint64_t             mUID;
   std::string          mPassword;
   std::string          mOTP;
   std::string          mSessionLabel;
   std::string          mAppToken;
   ConnectionMode       mConnectionMode;
   std::map<std::string, std::string>  mGameOptions;
   std::map<uint, uint> mGameSideInfos;
   uint                 mSide;
   uint64_t             mSubscribeTID;
   bool                 mIsManager;
   std::map<uint, clooponline::packet::SessionConfigs> mSessionsList;

   struct PlayerInfo
   {
      uint64_t UID;
      uint     Side;
      std::string NickName;
      std::string Avatar;
   };

   enum gameState
   {
      GAME_STATE_WAITING_FOR_START,
      GAME_STATE_START_COUNTDOWN,
      GAME_STATE_PLAYING,
      GAME_STATE_PAUSED,
      GAME_STATE_TERMINATED
   };

   //TODO: sync these
   std::vector<uint>    mAvailableActiveSessions;
   uint                 mPropertiesRevisionIndex;
   uint                 mLastRPCIndex;
   uint                 mLastCachedRPCIndex;

   std::map<uint64_t, PlayerInfo> mCoPlayers;
   gameState            mSessionState;

   bool mAlwaysAcceptJoinFlag;
   bool mAllShouldBeReadyFlag;
   bool mPlayersCanCancelFlag;
   bool mCanChangeSideAfterStartFlag;
   uint mDisconnectionTimeout;
   uint mMinPlayersToMatch;
   uint mStartTimeout;
   std::string mSessionPassword;
   clooponline::packet::DisconnectionActionType::type mDisconnectionAction;

protected:
   cxxtools::json::HttpClient* mAAAClient;
   RPCFunctions  mAAARPCFunctions;
   std::map<clooponline::packet::PacketType::type, std::condition_variable> mTypeCVs;

   virtual void gotManager(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotRPC(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotActiveSession(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotLock(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotResume(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotReady(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotStarted(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotRejoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotStartCanceled(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotPropertiesChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotSideChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotConfigChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotTempLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);

   virtual void subsDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void sessionListDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void createSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void handleActiveSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void RPCCallDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void setPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void connectedToServer(ProfilePtr server);

   void fillSessionConfigs(clooponline::packet::SessionConfigs& configs);
};