#pragma once

#include "catch.hpp"
#include <ThriftBuffer.h>

template <typename T>
class ITester
{
public:
   virtual void given() = 0;
   virtual void when() = 0;
   virtual void then() = 0;
   virtual void tearDown() = 0;

   static std::string given_description;
   static std::string when_description;
   static std::string then_description;
};

#define GIVEN_DESCRIPTION(TESTER_CLASS) \
template<> std::string ITester<TESTER_CLASS>::given_description

#define WHEN_DESCRIPTION(TESTER_CLASS) \
template<> std::string ITester<TESTER_CLASS>::when_description

#define THEN_DESCRIPTION(TESTER_CLASS) \
template<> std::string ITester<TESTER_CLASS>::then_description

#define DEFINE_TEST(TESTER_CLASS, SCENARIO_CATEGORY, SCENARIO_DESCRIPTION) \
SCENARIO(SCENARIO_DESCRIPTION, "[" SCENARIO_CATEGORY "]" ) \
{ \
   TESTER_CLASS tester; \
   GIVEN(TESTER_CLASS::given_description) \
   { \
      tester.given(); \
      WHEN(TESTER_CLASS::when_description) \
      { \
         tester.when(); \
         THEN(TESTER_CLASS::then_description) \
         { \
            tester.then(); \
         } \
      } \
   } \
   tester.tearDown(); \
}
