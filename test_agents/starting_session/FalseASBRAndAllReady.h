#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace FalseASBRAndAllReady
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotReady = mGotStarted = false;
      }

      bool mGotReady;
      bool mGotStarted;

      virtual void gotReady(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotReady = true;
         Bot::gotReady(server, reqPacket, repPacket);
      }

      virtual void gotStarted(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotStarted = true;
         Bot::gotStarted(server, reqPacket, repPacket);
      }
   };

   class FalseASBRAndAllReady : public Bot, public ITester<FalseASBRAndAllReady>
   {
   public:
      FalseASBRAndAllReady()
      {
         mGotReady = mGotStarted = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         b3.initialize("valid_configs.json", "bot3");
         Bot::login();
         b2.login();
         b3.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b3.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 2;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = b3.mSessionID = mSessionID;
         b2.sendJoinSession();
         b3.sendJoinSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mAllShouldBeReadyFlag = false;
         mStartTimeout = 10;
         sendStartSession();
      }

      virtual void gotReady(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotReady = true;
         Bot::gotReady(server, reqPacket, repPacket);
      }

      virtual void gotStarted(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotStarted = true;
         Bot::gotStarted(server, reqPacket, repPacket);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotReady);
         REQUIRE(mGotStarted);
         REQUIRE(b2.mGotReady);
         REQUIRE(b2.mGotStarted);
         REQUIRE(b3.mGotReady);
         REQUIRE(b3.mGotStarted);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         b3.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
         b3.disconnect();
      }

      Bot2 b2;
      Bot2 b3;
      bool mGotReady;
      bool mGotStarted;
   };

   GIVEN_DESCRIPTION(FalseASBRAndAllReady) = "three clients in a same session";
   WHEN_DESCRIPTION(FalseASBRAndAllReady) = "manager starts the game with all should be ready flag as false and all clients stated ready";
   THEN_DESCRIPTION(FalseASBRAndAllReady) = "the game should be started as soon as all the players stated ready";
   DEFINE_TEST(FalseASBRAndAllReady, "starting_session", "false ASBR and all ready")
}