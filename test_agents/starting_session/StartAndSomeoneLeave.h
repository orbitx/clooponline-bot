#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace StartAndSomeoneLeave
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mIsInformationCorrect = mGotCancel = mGotReady = mGotStarted = false;
      }

      bool mGotReady;
      bool mGotStarted;
      bool mGotCancel;
      bool mIsInformationCorrect;
      uint64_t mLeaverID;

      virtual void gotReady(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotReady = true;
         Bot::gotReady(server, reqPacket, repPacket);
      }

      virtual void gotStarted(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotStarted = true;
         Bot::gotStarted(server, reqPacket, repPacket);
      }

      virtual void gotStartCanceled(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotCancel = true;
         Bot::gotStartCanceled(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(StartCanceled)

         if (innerpack.reason == StartCancelReason::SOMEONE_LEFT &&
             innerpack.culprits.size() == 1 &&
             innerpack.culprits[0] == mLeaverID)
            mIsInformationCorrect = true;
      }
   };

   class StartAndSomeoneLeave : public Bot, public ITester<StartAndSomeoneLeave>
   {
   public:
      StartAndSomeoneLeave()
      {
         mGotCancel = mGotReady = mGotStarted = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         b3.initialize("valid_configs.json", "bot3");
         b2.mLeaverID = b3.mLeaverID = mUID;
         Bot::login();
         b2.login();
         b3.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b3.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 2;
         b2.sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b3.mSessionID = b2.mSessionID;
         sendJoinSession();
         b3.sendJoinSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mAllShouldBeReadyFlag = true;
         b2.mStartTimeout = 1;
         b2.sendStartSession();
      }

      virtual void gotReady(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotReady = true;
         sendLeave();
      }

      virtual void gotStarted(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotStarted = true;
         Bot::gotStarted(server, reqPacket, repPacket);
      }

      virtual void gotStartCanceled(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotCancel = true;
         Bot::gotStartCanceled(server, reqPacket, repPacket);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotReady);
         REQUIRE(!mGotStarted);
         REQUIRE(!mGotCancel);
         REQUIRE(b2.mGotReady);
         REQUIRE(!(b2.mGotStarted));
         REQUIRE(b2.mGotCancel);
         REQUIRE(b2.mIsInformationCorrect);
         REQUIRE(b3.mGotReady);
         REQUIRE(!(b3.mGotStarted));
         REQUIRE(b3.mGotCancel);
         REQUIRE(b3.mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         b2.sendLeave();
         b3.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
         b3.disconnect();
      }

      Bot2 b2;
      Bot2 b3;
      bool mGotReady;
      bool mGotStarted;
      bool mGotCancel;
   };

   GIVEN_DESCRIPTION(StartAndSomeoneLeave) = "three clients in a same session";
   WHEN_DESCRIPTION(StartAndSomeoneLeave) = "manager starts the game but someone leaves the session";
   THEN_DESCRIPTION(StartAndSomeoneLeave) = "the game should not start and all should be informed about this cancellation";
   DEFINE_TEST(StartAndSomeoneLeave, "starting_session", "start and someone leave")
}