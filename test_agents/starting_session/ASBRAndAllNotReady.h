#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ASBRAndAllNotReady
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotCancel = mGotReady = mGotStarted = false;
      }

      bool mGotReady;
      bool mGotStarted;
      bool mGotCancel;

      virtual void gotReady(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotReady = true;
         Bot::gotReady(server, reqPacket, repPacket);
      }

      virtual void gotStarted(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotStarted = true;
         Bot::gotStarted(server, reqPacket, repPacket);
      }

      virtual void gotStartCanceled(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotCancel = true;
         Bot::gotStartCanceled(server, reqPacket, repPacket);
      }
   };

   class ASBRAndAllNotReady : public Bot, public ITester<ASBRAndAllNotReady>
   {
   public:
      ASBRAndAllNotReady()
      {
         mIsInformationCorrect = mGotCancel = mGotReady = mGotStarted = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         b3.initialize("valid_configs.json", "bot3");
         Bot::login();
         b2.login();
         b3.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b3.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 2;
         b2.sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b3.mSessionID = b2.mSessionID;
         sendJoinSession();
         b3.sendJoinSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mAllShouldBeReadyFlag = true;
         b2.mStartTimeout = 1;
         b2.sendStartSession();
      }

      virtual void gotReady(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotReady = true;
      }

      virtual void gotStarted(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotStarted = true;
         Bot::gotStarted(server, reqPacket, repPacket);
      }

      virtual void gotStartCanceled(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotCancel = true;
         Bot::gotStartCanceled(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(StartCanceled);
         if (innerpack.reason == StartCancelReason::ALL_NOT_READY &&
             innerpack.culprits.size() == 1 &&
             innerpack.culprits[0] == mUID)
            mIsInformationCorrect = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotReady);
         REQUIRE(!mGotStarted);
         REQUIRE(mGotCancel);
         REQUIRE(mIsInformationCorrect);
         REQUIRE(b2.mGotReady);
         REQUIRE(!(b2.mGotStarted));
         REQUIRE(b2.mGotCancel);
         REQUIRE(b3.mGotReady);
         REQUIRE(!(b3.mGotStarted));
         REQUIRE(b3.mGotCancel);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         b3.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
         b3.disconnect();
      }

      Bot2 b2;
      Bot2 b3;
      bool mGotReady;
      bool mGotStarted;
      bool mGotCancel;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(ASBRAndAllNotReady) = "three clients in a same session";
   WHEN_DESCRIPTION(ASBRAndAllNotReady) = "manager starts the game with all should be ready flag as true and one of the other two does not state ready";
   THEN_DESCRIPTION(ASBRAndAllNotReady) = "the game should not start and all should be informed about this cancellation";
   DEFINE_TEST(ASBRAndAllNotReady, "starting_session", "ASBR and all not ready")
}