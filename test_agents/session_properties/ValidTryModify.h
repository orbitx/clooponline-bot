#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ValidTryModify
{
   class ValidTryModify : public Bot, public ITester<ValidTryModify>
   {
   public:
      ValidTryModify()
      {
         mIsPropertiesCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("modify", "test", 123, 0);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("try", "test", 12, 123);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 1)
            if (innerpack.properties.children.begin()->first == "test" &&
                innerpack.properties.children.begin()->second.int32Val == 12)
               mIsPropertiesCorrect = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot b2;
      bool mIsPropertiesCorrect;
   };

   GIVEN_DESCRIPTION(ValidTryModify) = "two clients in a same session and a set property";
   WHEN_DESCRIPTION(ValidTryModify) = "one of them try modifies that property with valid cached value";
   THEN_DESCRIPTION(ValidTryModify) = "it should be successful and the other one should be notified";
   DEFINE_TEST(ValidTryModify, "session_properties", "valid try modify property values")
}