#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace DeleteProperty
{
   class DeleteProperty : public Bot, public ITester<DeleteProperty>
   {
   public:
      DeleteProperty()
      {
         mGotCorrectResult = mIsPropertiesCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("modify", "test", 123, 0);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("delete", "test", 12, 123);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotCorrectResult);
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 0)
            mIsPropertiesCorrect = true;
      }

      virtual void gotPropertiesChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(PropertiesChanged)
         if (innerpack.changes.size() == 1 &&
             innerpack.changes[0].operation == OperationType::DELETE &&
             innerpack.changes[0].path == "test")
            mGotCorrectResult = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot b2;
      bool mIsPropertiesCorrect;
      bool mGotCorrectResult;
   };

   GIVEN_DESCRIPTION(DeleteProperty) = "two clients in a same session and a set property";
   WHEN_DESCRIPTION(DeleteProperty) = "one of them deletes that property";
   THEN_DESCRIPTION(DeleteProperty) = "it should be successful and the other one should be notified";
   DEFINE_TEST(DeleteProperty, "session_properties", "delete property values")
}