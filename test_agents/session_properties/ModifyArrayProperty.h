#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ModifyArrayProperty
{
   class ModifyArrayProperty : public Bot, public ITester<ModifyArrayProperty>
   {
   public:
      ModifyArrayProperty()
      {
         mIsPropertiesCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("modify", "test", std::vector<int>({1, 2, 3}));
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("modify", "test[1]", 12, 0);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 1)
            if (innerpack.properties.children.begin()->first == "test" &&
                innerpack.properties.children.begin()->second.array.size() == 3 &&
                innerpack.properties.children.begin()->second.array[0].int32Val == 1 &&
                innerpack.properties.children.begin()->second.array[1].int32Val == 12 &&
                innerpack.properties.children.begin()->second.array[2].int32Val == 3)
               mIsPropertiesCorrect = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot b2;
      bool mIsPropertiesCorrect;
   };

   GIVEN_DESCRIPTION(ModifyArrayProperty) = "two clients in a same session";
   WHEN_DESCRIPTION(ModifyArrayProperty) = "one of them modifies a member of an array in the session properties";
   THEN_DESCRIPTION(ModifyArrayProperty) = "it should be successful and the other one should be notified";
   DEFINE_TEST(ModifyArrayProperty, "session_properties", "modify array member from property")
}