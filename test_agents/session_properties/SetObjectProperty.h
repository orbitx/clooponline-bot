#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace SetObjectProperty
{
   class SetObjectProperty : public Bot, public ITester<SetObjectProperty>
   {
   public:
      SetObjectProperty()
      {
         mIsPropertiesCorrect = mGotProperty = mIsValueCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("modify", "test", {{"a", 1}, {"b", 2}, {"c", 3}});
      }

      virtual void gotPropertiesChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotPropertiesChanged(server, reqPacket, repPacket);
         HANDLE_METHOD_HEAD(PropertiesChanged)
         mGotProperty = true;
         if (innerpack.changes[0].path == "test" &&
             innerpack.changes[0].value.children["a"].int32Val == 1 &&
             innerpack.changes[0].value.children["b"].int32Val == 2 &&
             innerpack.changes[0].value.children["c"].int32Val == 3)
         {
            mIsValueCorrect = true;
         }
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotProperty);
         REQUIRE(mIsValueCorrect);
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 1)
            if (innerpack.properties.children.begin()->first == "test" &&
                innerpack.properties.children.begin()->second.children.size() == 3 &&
                innerpack.properties.children.begin()->second.children["a"].int32Val == 1 &&
                innerpack.properties.children.begin()->second.children["b"].int32Val == 2 &&
                innerpack.properties.children.begin()->second.children["c"].int32Val == 3)
               mIsPropertiesCorrect = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot b2;
      bool mGotProperty;
      bool mIsValueCorrect;
      bool mIsPropertiesCorrect;
   };

   GIVEN_DESCRIPTION(SetObjectProperty) = "two clients in a same session";
   WHEN_DESCRIPTION(SetObjectProperty) = "one of them sets a children of an object in the session properties";
   THEN_DESCRIPTION(SetObjectProperty) = "it should be successful and the other one should be notified";
   DEFINE_TEST(SetObjectProperty, "session_properties", "set object property")
}