#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace InvalidTryModify
{
   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotCorrectResult = false;
      }

      bool mGotCorrectResult;

      virtual void setPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(SetPropertiesResponse)
         if (innerpack.resultFlags.size() == 1 &&
             innerpack.resultFlags[0] == SetPropertyResult::ERROR_INVALID_CACHED_VALUE)
            mGotCorrectResult = true;
      }
   };

   class InvalidTryModify : public Bot, public ITester<InvalidTryModify>
   {
   public:
      InvalidTryModify()
      {
         mIsPropertiesCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("modify", "test", 123, 0);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("try", "test", 12, 3);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mGotCorrectResult);
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 1)
            if (innerpack.properties.children.begin()->first == "test" &&
                innerpack.properties.children.begin()->second.int32Val == 123)
               mIsPropertiesCorrect = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
      bool mIsPropertiesCorrect;
   };

   GIVEN_DESCRIPTION(InvalidTryModify) = "two clients in a same session and a set property";
   WHEN_DESCRIPTION(InvalidTryModify) = "one of them try modifies that property with invalid cached value";
   THEN_DESCRIPTION(InvalidTryModify) = "it should not be successful and the setter client should receive an invalid cache result";
   DEFINE_TEST(InvalidTryModify, "session_properties", "invalid try modify property values")
}