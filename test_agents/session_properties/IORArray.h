#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace IORArray
{
   class IORArray : public Bot, public ITester<IORArray>
   {
   public:
      IORArray()
      {
         mGotCorrectResult = mIsPropertiesCorrect = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         login();
         connectToGameServer();
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendSetProperty("modify", "test", std::vector<int>{1, 2, 3});
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendSetProperty("modify", "test[5]", 12, 0);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotCorrectResult);
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 1)
            if (innerpack.properties.children.begin()->first == "test" &&
                innerpack.properties.children.begin()->second.array.size() == 3 &&
                innerpack.properties.children.begin()->second.array[0].int32Val == 1 &&
                innerpack.properties.children.begin()->second.array[1].int32Val == 2 &&
                innerpack.properties.children.begin()->second.array[2].int32Val == 3)
               mIsPropertiesCorrect = true;
      }

      virtual void setPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(SetPropertiesResponse)
         if (innerpack.resultFlags.size() == 1 &&
             innerpack.resultFlags[0] == SetPropertyResult::ERROR_INDEX_OUT_OF_RANGE)
            mGotCorrectResult = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      bool mIsPropertiesCorrect;
      bool mGotCorrectResult;
   };

   GIVEN_DESCRIPTION(IORArray) = "a client in a session";
   WHEN_DESCRIPTION(IORArray) = "he modifies an array but with out of ranged index";
   THEN_DESCRIPTION(IORArray) = "it should receive an error in result";
   DEFINE_TEST(IORArray, "session_properties", "array index out of range")
}