#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ModifyObjectProperty
{
   class ModifyObjectProperty : public Bot, public ITester<ModifyObjectProperty>
   {
   public:
      ModifyObjectProperty()
      {
         mIsPropertiesCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("modify", "test", {{"a", 1}, {"b", 2}, {"c", 3}});
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("modify", "test.b", 12, 0);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 1)
            if (innerpack.properties.children.begin()->first == "test" &&
                innerpack.properties.children.begin()->second.children.size() == 3 &&
                innerpack.properties.children.begin()->second.children["a"].int32Val == 1 &&
                innerpack.properties.children.begin()->second.children["b"].int32Val == 12 &&
                innerpack.properties.children.begin()->second.children["c"].int32Val == 3)
               mIsPropertiesCorrect = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot b2;
      bool mIsPropertiesCorrect;
   };

   GIVEN_DESCRIPTION(ModifyObjectProperty) = "two clients in a same session";
   WHEN_DESCRIPTION(ModifyObjectProperty) = "one of them modifies a child of an object in the session properties";
   THEN_DESCRIPTION(ModifyObjectProperty) = "it should be successful and the other one should be notified";
   DEFINE_TEST(ModifyObjectProperty, "session_properties", "modify a child from a properties object")
}