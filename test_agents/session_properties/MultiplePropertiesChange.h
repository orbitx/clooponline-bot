#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace MultiplePropertiesChange
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotCorrectResult = false;
      }

      bool mGotCorrectResult;

      virtual void gotPropertiesChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(PropertiesChanged)
         if (innerpack.changes.size() == 2 &&
             innerpack.changes[0].path == "test1" &&
             innerpack.changes[0].value.int32Val == 123 &&
             innerpack.changes[1].path == "test2" &&
             innerpack.changes[1].value.int32Val == 321)
            mGotCorrectResult = true;
      }
   };

   class MultiplePropertiesChange : public Bot, public ITester<MultiplePropertiesChange>
   {
   public:
      MultiplePropertiesChange()
      {
         mGotCorrectResult = mIsPropertiesCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         auto set = (ThriftParser*)createPacket();
         set->setMessageType(PacketType::SET_PROPERTIES);
         set->pSetProperties.__set_sessionID(mSessionID);
         {
            PropertyInstance p;
            p.__set_operation(OperationType::MODIFY);
            p.__set_path("test1");
            p.__isset.value = true;
            p.value.type = ValueType::INT_32;
            p.value.__set_int32Val(123);
            set->pSetProperties.properties.push_back(p);
         }
         {
            PropertyInstance p;
            p.__set_operation(OperationType::MODIFY);
            p.__set_path("test2");
            p.__isset.value = true;
            p.value.type = ValueType::INT_32;
            p.value.__set_int32Val(321);
            set->pSetProperties.properties.push_back(p);
         }
         sendRequest(mServer, set, bind(&MultiplePropertiesChange::setPropertiesDone, this, _1, _2, _3));
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotCorrectResult);
         REQUIRE(b2.mGotCorrectResult);
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void setPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(SetPropertiesResponse)
         if (innerpack.resultFlags.size() == 2 &&
             innerpack.resultFlags[0] == SetPropertyResult::OK &&
             innerpack.resultFlags[1] == SetPropertyResult::OK)
            mGotCorrectResult = true;
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 2 &&
             innerpack.properties.children["test1"].int32Val == 123 &&
             innerpack.properties.children["test2"].int32Val == 321)
            mIsPropertiesCorrect = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
      bool mIsPropertiesCorrect;
      bool mGotCorrectResult;
   };

   GIVEN_DESCRIPTION(MultiplePropertiesChange) = "two clients in a same session";
   WHEN_DESCRIPTION(MultiplePropertiesChange) = "one of them changes multiple properties in one packet";
   THEN_DESCRIPTION(MultiplePropertiesChange) = "it should be successful and the other one should be notified";
   DEFINE_TEST(MultiplePropertiesChange, "session_properties", "multiple properties change")
}