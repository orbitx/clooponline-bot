#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace SetValueProperty
{
   class SetValueProperty : public Bot, public ITester<SetValueProperty>
   {
   public:
      SetValueProperty()
      {
         mIsPropertiesCorrect = mGotProperty = mIsValueCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSetProperty("modify", "test", 123, 0);
      }

      virtual void gotPropertiesChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotPropertiesChanged(server, reqPacket, repPacket);
         HANDLE_METHOD_HEAD(PropertiesChanged)
         mGotProperty = true;
         if (innerpack.changes[0].path == "test" && innerpack.changes[0].value.int32Val == 123)
            mIsValueCorrect = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotProperty);
         REQUIRE(mIsValueCorrect);
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 1)
            if (innerpack.properties.children.begin()->first == "test" &&
                innerpack.properties.children.begin()->second.int32Val == 123)
               mIsPropertiesCorrect = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot b2;
      bool mGotProperty;
      bool mIsValueCorrect;
      bool mIsPropertiesCorrect;
   };

   GIVEN_DESCRIPTION(SetValueProperty) = "two clients in a same session";
   WHEN_DESCRIPTION(SetValueProperty) = "one of them sets a value as a property object";
   THEN_DESCRIPTION(SetValueProperty) = "it should be successful and the other one should be notified";
   DEFINE_TEST(SetValueProperty, "session_properties", "set value property")
}