#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangePropertyType
{
   class ChangePropertyType : public Bot, public ITester<ChangePropertyType>
   {
   public:
      ChangePropertyType()
      {
         mGotCorrectResult = mIsPropertiesCorrect = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         login();
         connectToGameServer();
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendSetProperty("modify", "test", 123, 0);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendSetProperty("modify", "test", "change");
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotCorrectResult);
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 1 &&
             innerpack.properties.children["test"].strVal == "change")
            mIsPropertiesCorrect = true;
      }

      virtual void setPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(SetPropertiesResponse)
         static bool firstTime = true;

         if (firstTime)
            firstTime = false;
         else if (innerpack.resultFlags.size() == 1 &&
                  innerpack.resultFlags[0] == SetPropertyResult::WARNING_TYPE_CHANGED)
            mGotCorrectResult = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      bool mIsPropertiesCorrect;
      bool mGotCorrectResult;
   };

   GIVEN_DESCRIPTION(ChangePropertyType) = "a client in a session with a set property";
   WHEN_DESCRIPTION(ChangePropertyType) = "he modifies that property which changes its type";
   THEN_DESCRIPTION(ChangePropertyType) = "it should be successful but result should contain a warning";
   DEFINE_TEST(ChangePropertyType, "session_properties", "changing property type")
}