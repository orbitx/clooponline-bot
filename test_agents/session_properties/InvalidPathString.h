#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace InvalidPathString
{
   class InvalidPathString : public Bot, public ITester<InvalidPathString>
   {
   public:
      InvalidPathString()
      {
         mGotCorrectResult = mIsPropertiesCorrect = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         login();
         connectToGameServer();
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendSetProperty("modify", "test[", 123, 0);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotCorrectResult);
         sendGetProperty();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mIsPropertiesCorrect);
      }

      virtual void getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(GetPropertiesResponse)
         if (innerpack.properties.children.size() == 0)
            mIsPropertiesCorrect = true;
      }

      virtual void setPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(SetPropertiesResponse)
         if (innerpack.resultFlags.size() == 1 &&
             innerpack.resultFlags[0] == SetPropertyResult::ERROR_PATH_SYNTAX_ERROR)
            mGotCorrectResult = true;
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      bool mIsPropertiesCorrect;
      bool mGotCorrectResult;
   };

   GIVEN_DESCRIPTION(InvalidPathString) = "a client in a session";
   WHEN_DESCRIPTION(InvalidPathString) = "he modifies a property but the path string is invalid";
   THEN_DESCRIPTION(InvalidPathString) = "it should receive an error in result";
   DEFINE_TEST(InvalidPathString, "session_properties", "invalid path string")
}