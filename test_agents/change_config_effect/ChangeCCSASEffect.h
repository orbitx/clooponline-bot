#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangeCCSASEffect
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSideChanged = false;
      }

      bool mGotSideChanged;

      virtual void gotSideChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotSideChanged = true;
      }
   };

   class ChangeCCSASEffect : public Bot, public ITester<ChangeCCSASEffect>
   {
   public:
      ChangeCCSASEffect()
      {
         mGotException = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.mSide = 1;
         b2.mCanChangeSideAfterStartFlag = true;
         b2.sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         mSide = 2;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendStartSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_canChangeSideAfterStart(false);
         b2.sendChangeConfig(sc);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendChangeSide(0);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotException);
         REQUIRE(!(b2.mGotSideChanged));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::INVALID_SESSION_STATE)
            mGotException = true;
      }

      Bot2 b2;
      bool mGotException;
   };

   GIVEN_DESCRIPTION(ChangeCCSASEffect) = "two clients in a same session which is started with two sides each with capacity as 2 and CCSAS flag as true";
   WHEN_DESCRIPTION(ChangeCCSASEffect) = "manager changes CCSAS flag to false then the client who is not manager attempts to change its side";
   THEN_DESCRIPTION(ChangeCCSASEffect) = "it should not be successful and an exception should be raised";
   DEFINE_TEST(ChangeCCSASEffect, "change_config_effect", "change can change side after start effect")
}