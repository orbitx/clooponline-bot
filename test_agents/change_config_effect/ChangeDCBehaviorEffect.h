#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangeDCBehaviorEffect
{
   class ChangeDCBehaviorEffect : public Bot, public ITester<ChangeDCBehaviorEffect>
   {
   public:
      ChangeDCBehaviorEffect()
      {
         mGotLeft = mGotTerminate = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mDisconnectionAction = DisconnectionActionType::TERMINATE;
         mDisconnectionTimeout = 1;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_disconnectionBehavior(DisconnectionActionType::KICK);
         sendChangeConfig(sc);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.disconnect();
      }

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotLeft = true;
      }

      virtual void gotTerminated(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotTerminate = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotLeft);
         REQUIRE(!mGotTerminate);
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      Bot  b2;
      bool mGotLeft;
      bool mGotTerminate;
   };

   GIVEN_DESCRIPTION(ChangeDCBehaviorEffect) = "two clients in a same session which is started with disconnection behavior set as terminate";
   WHEN_DESCRIPTION(ChangeDCBehaviorEffect) = "manager changes dc behavior to kick then the client who is not manager disconnects";
   THEN_DESCRIPTION(ChangeDCBehaviorEffect) = "he should be kicked and the session should not be terminated";
   DEFINE_TEST(ChangeDCBehaviorEffect, "change_config_effect", "change disconnection behavior effect")
}