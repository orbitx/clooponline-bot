#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangeDCTimeoutEffect
{
   class ChangeDCTimeoutEffect : public Bot, public ITester<ChangeDCTimeoutEffect>
   {
   public:
      ChangeDCTimeoutEffect()
      {
         mGotLeft = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mDisconnectionAction = DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT;
         mDisconnectionTimeout = 30;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_disconnectionTimeout(1);
         sendChangeConfig(sc);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.disconnect();
      }

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotLeft = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotLeft);
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      Bot  b2;
      bool mGotLeft;
   };

   GIVEN_DESCRIPTION(ChangeDCTimeoutEffect) = "two clients in a same session which is started with disconnection behavior set as continue and kick and timeout as 30 seconds";
   WHEN_DESCRIPTION(ChangeDCTimeoutEffect) = "manager changes dc timeout to be 1 second then the client who is not manager disconnects";
   THEN_DESCRIPTION(ChangeDCTimeoutEffect) = "he should be kicked after 1 second";
   DEFINE_TEST(ChangeDCTimeoutEffect, "change_config_effect", "change disconnection timeout effect")
}