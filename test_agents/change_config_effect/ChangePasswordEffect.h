#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangePasswordEffect
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotException = mJoined = false;
      }

      bool mJoined;
      bool mGotException;

      virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mJoined = true;
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::WRONG_OPTIONS)
            mGotException = true;
      }
   };

   class ChangePasswordEffect : public Bot, public ITester<ChangePasswordEffect>
   {
   public:
      ChangePasswordEffect()
      {
         mGotJoined = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mSide = 1;
         mPassword = "123";
         sendCreateSession();
      }

      virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotJoined = true;
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_password("123123");
         sendChangeConfig(sc);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mPassword = "123";
         b2.sendJoinSession();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotJoined);
         REQUIRE(b2.mGotException);
         REQUIRE(!(b2.mJoined));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      Bot2 b2;
      bool mGotJoined;
   };

   GIVEN_DESCRIPTION(ChangePasswordEffect) = "A clients in a session which has an password set to “123”";
   WHEN_DESCRIPTION(ChangePasswordEffect) = "manager changes that password to be “123123” then another client attempts to connect with password as “123”";
   THEN_DESCRIPTION(ChangePasswordEffect) = "it should not be successful and an exception should be raised";
   DEFINE_TEST(ChangePasswordEffect, "change_config_effect", "change password effect")
}