#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangeCapacityEffect
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotException = mJoined = false;
      }

      bool mJoined;
      bool mGotException;

      virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mJoined = true;
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)
         if (innerpack.errorCode == ExceptionCode::WRONG_OPTIONS)
            mGotException = true;
      }
   };

   class ChangeCapacityEffect : public Bot, public ITester<ChangeCapacityEffect>
   {
   public:
      ChangeCapacityEffect()
      {
         mGotJoined = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 2;
         mGameSideInfos[2] = 2;
         mSide = 1;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_sideToCapacity(std::map<int, int>({{1, 1}}));
         sendChangeConfig(sc);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mSide = 2;
         b2.sendJoinSession();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotJoined);
         REQUIRE(b2.mGotException);
         REQUIRE(!(b2.mJoined));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotJoined = true;
      }

      Bot2 b2;
      bool mGotJoined;
   };

   GIVEN_DESCRIPTION(ChangeCapacityEffect) = "a client who created a session with two sides each with capacity as 2 and that client himself took side 1";
   WHEN_DESCRIPTION(ChangeCapacityEffect) = "that client changes the capacity of session to have only side 1 with capacity 1 then another client attempts to join side 2";
   THEN_DESCRIPTION(ChangeCapacityEffect) = "it should not be successful and an exception should be raised";
   DEFINE_TEST(ChangeCapacityEffect, "change_config_effect", "change capacity effect")
}