#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangeAAJEffect
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotException = mGotJoinSessionResponse = false;
      }

      bool mGotJoinSessionResponse;
      bool mGotException;

      virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::joinSessionDone(server, reqPacket, repPacket);
         mGotJoinSessionResponse = true;
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::WRONG_OPTIONS)
            mGotException = true;
      }
   };

   class ChangeAAJEffect : public Bot, public ITester<ChangeAAJEffect>
   {
   public:
      ChangeAAJEffect()
      {
         mGotJoined = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mAlwaysAcceptJoinFlag = true;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_alwaysAcceptJoin(false);
         sendChangeConfig(sc);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotJoined);
         REQUIRE(b2.mGotException);
         REQUIRE(!(b2.mGotJoinSessionResponse));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotJoined(server, reqPacket, repPacket);
         mGotJoined = true;
      }

      Bot2 b2;
      bool mGotJoined;
   };

   GIVEN_DESCRIPTION(ChangeAAJEffect) = "a client who created a session with two sides each with capacity as 1 and that client himself took side 1 and set AAJ flag to true";
   WHEN_DESCRIPTION(ChangeAAJEffect) = "that client starts the session then changes the changes the AAJ flag to false and then another tries to join the session";
   THEN_DESCRIPTION(ChangeAAJEffect) = "it should not be successful and an exception should be raised";
   DEFINE_TEST(ChangeAAJEffect, "change_config_effect", "change always accept join effect")

}