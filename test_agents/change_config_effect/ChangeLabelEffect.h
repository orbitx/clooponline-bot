#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangeLabelEffect
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mIsInformationCorrect = false;
      }

      bool mIsInformationCorrect;

      virtual void sessionListDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(SessionListResponse)

         if (innerpack.IDtoConfigs.size() == 1 &&
             innerpack.IDtoConfigs.begin()->second.label == "YourGame")
            mIsInformationCorrect = true;
      }
   };

   class ChangeLabelEffect : public Bot, public ITester<ChangeLabelEffect>
   {
   public:
      ChangeLabelEffect()
      {
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mSide = 1;
         mSessionLabel = "MyGame";
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_label("YourGame");
         sendChangeConfig(sc);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSessionList();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      Bot2 b2;
   };

   GIVEN_DESCRIPTION(ChangeLabelEffect) = "A clients in a session which has label set as “MyGame”";
   WHEN_DESCRIPTION(ChangeLabelEffect) = "manager changes that label to be “YourGame” then another client gets the list of sessions";
   THEN_DESCRIPTION(ChangeLabelEffect) = "its label should be “YourGame”";
   DEFINE_TEST(ChangeLabelEffect, "change_config_effect", "change label effect")
}