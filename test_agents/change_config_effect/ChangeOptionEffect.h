#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangeOptionEffect
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mIsInformationCorrect = false;
      }

      bool mIsInformationCorrect;

      virtual void sessionListDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(SessionListResponse)

         if (innerpack.IDtoConfigs.size() == 1 &&
             innerpack.IDtoConfigs.begin()->second.options["op1"] == "false")
            mIsInformationCorrect = true;
      }
   };

   class ChangeOptionEffect : public Bot, public ITester<ChangeOptionEffect>
   {
   public:
      ChangeOptionEffect()
      {
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mSide = 1;
         mGameOptions["op1"] = "true";
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_options(std::map<std::string, std::string>({{"op1", "false"}}));
         sendChangeConfig(sc);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSessionList();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      Bot2 b2;
   };

   GIVEN_DESCRIPTION(ChangeOptionEffect) = "A clients in a session which has an option “op1” as “true”";
   WHEN_DESCRIPTION(ChangeOptionEffect) = "manager changes that option to be “false” then another gets the list of sessions";
   THEN_DESCRIPTION(ChangeOptionEffect) = "that session should have option “op1” set as “false”";
   DEFINE_TEST(ChangeOptionEffect, "change_config_effect", "change options effect")
}