#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ValidJoin
{

class Bot2 : public Bot
{
public:
   Bot2()
   {
      mGotJoinSessionResponse = false;
   }

   bool mGotJoinSessionResponse;

   virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
   {
      Bot::joinSessionDone(server, reqPacket, repPacket);
      mGotJoinSessionResponse = true;
   }
};

class ValidJoin : public Bot, public ITester<ValidJoin>
{
public:
   ValidJoin()
   {
      mGotJoined = false;
   }

   virtual void given()
   {
      Bot::initialize("valid_configs.json", "bot1");
      b2.initialize("valid_configs.json", "bot2");
      Bot::login();
      b2.login();
      Bot::connectToGameServer();
      b2.connectToGameServer();
      mGameSideInfos[1] = 1;
      mGameSideInfos[2] = 1;
      mGameOptions["a"] = "1";
      mGameOptions["b"] = "2";
      mAlwaysAcceptJoinFlag = mCanChangeSideAfterStartFlag = true;
      mDisconnectionAction = DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT;
      mDisconnectionTimeout = 10;
      mSessionLabel = "test_session";
      Bot::sendCreateSession();
      std::this_thread::sleep_for(std::chrono::seconds(1));
      Bot::sendRPC(0, true);
      Bot::sendSetProperty("modify", "a", 1, 0);
   }

   virtual void when()
   {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      b2.mSessionID = mSessionID;
      b2.sendJoinSession();
   }

   virtual void then()
   {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      REQUIRE(mGotJoined);
      REQUIRE(b2.mGotJoinSessionResponse);
      REQUIRE(b2.mSessionID == mSessionID);
      REQUIRE(b2.mGameOptions == mGameOptions);
      REQUIRE(b2.mLastRPCIndex == 1);
      REQUIRE(b2.mPropertiesRevisionIndex == 1);
      REQUIRE(b2.mAlwaysAcceptJoinFlag);
      REQUIRE(b2.mCanChangeSideAfterStartFlag);
      REQUIRE(b2.mDisconnectionAction == DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT);
      REQUIRE(b2.mDisconnectionTimeout == 10);
      REQUIRE(b2.mSessionLabel == "test_session");
   }

   virtual void tearDown()
   {
      Bot::disconnect();
      b2.disconnect();
   }

   virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
   {
      Bot::gotJoined(server, reqPacket, repPacket);
      mGotJoined = true;
   }

   Bot2 b2;
   bool mGotJoined;
};

GIVEN_DESCRIPTION(ValidJoin) = "two clients and identified connections to server and a valid created session by one of them";
WHEN_DESCRIPTION(ValidJoin) = "the second client attempts to join that session";
THEN_DESCRIPTION(ValidJoin) = "the second one should get a valid JoinSessionResponse packet and the first one should receive a valid Joined packet";
DEFINE_TEST(ValidJoin, "manual_session", "valid join to a manual session")

}