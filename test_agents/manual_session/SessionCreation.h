#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

class SessionCreation : public Bot, public ITester<SessionCreation>
{
public:
   SessionCreation()
   {
      mGotResponse = false;
   }

   virtual void given()
   {
      Bot::initialize("valid_configs.json", "bot1");
      Bot::login();
      Bot::connectToGameServer();
   }

   virtual void when()
   {
      Bot::sendCreateSession();
   }

   virtual void then()
   {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      REQUIRE(mGotResponse);
   }

   virtual void tearDown()
   {
      Bot::disconnect();
   }

   virtual void createSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
   {
      Bot::createSessionDone(server, reqPacket, repPacket);
      mGotResponse = true;
   }

   bool mGotResponse;
};

GIVEN_DESCRIPTION(SessionCreation) = "a client and an identified connection to server";
WHEN_DESCRIPTION(SessionCreation) = "the client sends a CreateSession packet";
THEN_DESCRIPTION(SessionCreation) = "it should be provided with a sessionID in the response";
DEFINE_TEST(SessionCreation, "manual_session", "manual session creation")