#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace JoinFalseAAJ
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotException = mGotJoinSessionResponse = false;
      }

      bool mGotJoinSessionResponse;
      bool mGotException;

      virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::joinSessionDone(server, reqPacket, repPacket);
         mGotJoinSessionResponse = true;
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::WRONG_OPTIONS)
            mGotException = true;
      }
   };

   class JoinFalseAAJ : public Bot, public ITester<JoinFalseAAJ>
   {
   public:
      JoinFalseAAJ()
      {
         mGotJoined = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mAlwaysAcceptJoinFlag = false;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotJoined);
         REQUIRE(b2.mGotException);
         REQUIRE(!(b2.mGotJoinSessionResponse));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotJoined(server, reqPacket, repPacket);
         mGotJoined = true;
      }

      Bot2 b2;
      bool mGotJoined;
   };

   GIVEN_DESCRIPTION(JoinFalseAAJ) = "a client in a session which has been started and has always accept join flag as false";
   WHEN_DESCRIPTION(JoinFalseAAJ) = "a second client attempts to join that session";
   THEN_DESCRIPTION(JoinFalseAAJ) = "it should not be successful and the attempt should result in an exception";
   DEFINE_TEST(JoinFalseAAJ, "manual_session", "join after start with AAJ as false")

}