#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ValidPasswordJoin
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotJoinSessionResponse = false;
      }

      bool mGotJoinSessionResponse;

      virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::joinSessionDone(server, reqPacket, repPacket);
         mGotJoinSessionResponse = true;
      }
   };

   class ValidPasswordJoin : public Bot, public ITester<ValidPasswordJoin>
   {
   public:
      ValidPasswordJoin()
      {
         mGotJoined = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mSessionPassword = "123123";
         Bot::sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mSessionPassword = mSessionPassword;
         b2.sendJoinSession();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotJoined);
         REQUIRE(b2.mGotJoinSessionResponse);
      }

      virtual void tearDown()
      {
         Bot::disconnect();
         b2.disconnect();
      }

      virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotJoined(server, reqPacket, repPacket);
         mGotJoined = true;
      }

      Bot2 b2;
      bool mGotJoined;
   };

   GIVEN_DESCRIPTION(ValidPasswordJoin) = "two clients and identified connections to server and a valid created session by one of them";
   WHEN_DESCRIPTION(ValidPasswordJoin) = "the second client attempts to join that session with valid password";
   THEN_DESCRIPTION(ValidPasswordJoin) = "the second one should get a valid JoinSessionResponse packet and the first one should receive a valid Joined packet";
   DEFINE_TEST(ValidPasswordJoin, "manual_session", "valid password join to a manual session")

}