#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace InvalidPasswordJoin
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotJoinSessionResponse = false;
         mExceptionRaised = false;
      }

      bool mGotJoinSessionResponse;
      bool mExceptionRaised;

      virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::joinSessionDone(server, reqPacket, repPacket);
         mGotJoinSessionResponse = true;
      }

      void sendInvalidJoin()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         auto join = (ThriftParser*)createPacket();
         join->setMessageType(PacketType::JOIN_SESSION);
         join->pJoinSession.__set_sessionID(mSessionID);
         join->pJoinSession.__set_side(mSide);
         join->pJoinSession.__set_password(mSessionPassword);
         sendRequest(mServer, join, bind(&Bot2::joinSessionDone, this, _1, _2, _3),
                     [this](ProfilePtr server2, PacketParser* reqPacket, PacketParser* repPacket)
                     {
                        HANDLE_METHOD_HEAD(Exception)
                        if (innerpack.errorCode == ExceptionCode::WRONG_OPTIONS)
                           mExceptionRaised = true;
                     });
      }
   };

   class InvalidPasswordJoin : public Bot, public ITester<InvalidPasswordJoin>
   {
   public:
      InvalidPasswordJoin()
      {
         mGotJoined = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mSessionPassword = "123123";
         Bot::sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mSessionPassword = "123";
         b2.sendInvalidJoin();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotJoined);
         REQUIRE(!(b2.mGotJoinSessionResponse));
         REQUIRE(b2.mExceptionRaised);
      }

      virtual void tearDown()
      {
         Bot::disconnect();
         b2.disconnect();
      }

      virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotJoined(server, reqPacket, repPacket);
         mGotJoined = true;
      }

      Bot2 b2;
      bool mGotJoined;
   };

   GIVEN_DESCRIPTION(InvalidPasswordJoin) = "two clients and identified connections to server and a valid created session by one of them";
   WHEN_DESCRIPTION(InvalidPasswordJoin) = "the second client attempts to join that session but with invalid password";
   THEN_DESCRIPTION(InvalidPasswordJoin) = "the second one should get an Exception packet and the first one should not receive anything";
   DEFINE_TEST(InvalidPasswordJoin, "manual_session", "invalid password join to a manual session")

}