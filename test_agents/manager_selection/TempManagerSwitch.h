#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace TempManagerSwitch
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotBeManager = false;
      }

      bool mGotBeManager;

      virtual void gotManager(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotBeManager = true;
         Bot::gotManager(server, reqPacket, repPacket);
      }
   };

   class TempManagerSwitch : public Bot, public ITester<TempManagerSwitch>
   {
   public:
      TempManagerSwitch()
      {
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mDisconnectionAction = DisconnectionActionType::KICK;
         Bot::sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mAllShouldBeReadyFlag = false;
         mStartTimeout = 0;
         sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendTempLeave();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mGotBeManager);
      }

      virtual void tearDown()
      {
         sendHandleActiveSession(mSessionID, "leave");
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
   };

   GIVEN_DESCRIPTION(TempManagerSwitch) = "two clients which are in a same session";
   WHEN_DESCRIPTION(TempManagerSwitch) = "the manager temporarily leaves the session";
   THEN_DESCRIPTION(TempManagerSwitch) = "the second client should receive a BeManager packet";
   DEFINE_TEST(TempManagerSwitch, "manager_selection", "manager switch on temporary leave")
}