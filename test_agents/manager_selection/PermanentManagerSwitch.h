#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace PermanentManagerSwitch
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotBeManager = false;
      }

      bool mGotBeManager;

      virtual void gotManager(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotManager(server, reqPacket, repPacket);
         mGotBeManager = true;
      }
   };

   class PermanentManagerSwitch : public Bot, public ITester<PermanentManagerSwitch>
   {
   public:
      PermanentManagerSwitch()
      {
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         Bot::sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         sendLeave();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mGotBeManager);
      }

      virtual void tearDown()
      {
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
   };

   GIVEN_DESCRIPTION(PermanentManagerSwitch) = "two clients which are in a same session";
   WHEN_DESCRIPTION(PermanentManagerSwitch) = "the manager permanently leaves the session";
   THEN_DESCRIPTION(PermanentManagerSwitch) = "the second client should receive a BeManager packet";
   DEFINE_TEST(PermanentManagerSwitch, "manager_selection", "manager switch on permanent leave")

}