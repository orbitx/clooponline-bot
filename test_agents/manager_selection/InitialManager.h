#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

class InitialManager : public Bot, public ITester<InitialManager>
{
public:
   InitialManager()
   {
      mGotBeManager = false;
   }

   virtual void given()
   {
      Bot::initialize("valid_configs.json", "bot1");
      Bot::login();
      Bot::connectToGameServer();
   }

   virtual void when()
   {
      Bot::sendCreateSession();
   }

   virtual void then()
   {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      REQUIRE(mGotBeManager);
   }

   virtual void tearDown()
   {
      Bot::disconnect();
   }

   virtual void gotManager(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
   {
      Bot::gotManager(server, reqPacket, repPacket);
      mGotBeManager = true;
   }

   bool mGotBeManager;
};

GIVEN_DESCRIPTION(InitialManager) = "a client and an identified connection to server";
WHEN_DESCRIPTION(InitialManager) = "the client sends a CreateSession packet";
THEN_DESCRIPTION(InitialManager) = "it should immediately receive a BeManager packet";
DEFINE_TEST(InitialManager, "manager_selection", "initial manager")