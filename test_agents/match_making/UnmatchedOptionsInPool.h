#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace UnmatchedOptionsInPool
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSubsResult = false;
      }

      bool mGotSubsResult;

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }
   };

   class UnmatchedOptionsInPool : public Bot, public ITester<UnmatchedOptionsInPool>
   {
   public:
      UnmatchedOptionsInPool()
      {
         mGotSubsResult = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         b3.initialize("valid_configs.json", "bot3");
         login();
         b2.login();
         b3.login();
         connectToGameServer();
         b2.connectToGameServer();
         b3.connectToGameServer();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mGameOptions["op1"] = "true";
         sendSubs(1, 10);

         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.mGameOptions["op1"] = "false";
         b2.sendSubs(2, 10);

         b3.mGameSideInfos[1] = 1;
         b3.mGameSideInfos[2] = 1;
         b3.mGameOptions["op1"] = "true";
         b3.sendSubs(3, 1);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotSubsResult);
         REQUIRE(b3.mGotSubsResult);
         REQUIRE(!(b2.mGotSubsResult));
         REQUIRE(mSessionID == b3.mSessionID);
      }

      virtual void tearDown()
      {
         disconnect();
         b2.disconnect();
         b3.disconnect();
      }

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }

      Bot2 b2;
      Bot2 b3;
      bool mGotSubsResult;
   };

   GIVEN_DESCRIPTION(UnmatchedOptionsInPool) = "A client with indicator 1 and option “op1” as “true” and another client with indicator 2 and option “op1” as “false”";
   WHEN_DESCRIPTION(UnmatchedOptionsInPool) = "another client sends a subscribe packet with indicator 3 but option “op1” as “true”";
   THEN_DESCRIPTION(UnmatchedOptionsInPool) = "indicator 1 and 3 should be matched";
   DEFINE_TEST(UnmatchedOptionsInPool, "match_making", "not matched options in pool")
}