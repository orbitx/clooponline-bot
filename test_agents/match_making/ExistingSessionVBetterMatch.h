#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ExistingSessionVBetterMatch
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotJoined = mGotSubsResult = false;
      }

      bool mGotSubsResult;
      bool mGotJoined;

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }

      virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotJoined(server, reqPacket, repPacket);
         mGotJoined = true;
      }
   };

   class ExistingSessionVBetterMatch : public Bot, public ITester<ExistingSessionVBetterMatch>
   {
   public:
      ExistingSessionVBetterMatch()
      {
         mGotSubsResult = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         b3.initialize("valid_configs.json", "bot3");
         b4.initialize("valid_configs.json", "bot4");
         login();
         b2.login();
         b3.login();
         b4.login();
         connectToGameServer();
         b2.connectToGameServer();
         b3.connectToGameServer();
         b4.connectToGameServer();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 2;
         b2.mGameOptions["op1"] = "true";
         b2.mMinPlayersToMatch = 2;
         b2.mAlwaysAcceptJoinFlag = true;
         b2.sendSubs(1, 0);

         b3.mGameSideInfos[1] = 1;
         b3.mGameSideInfos[2] = 2;
         b3.mGameOptions["op1"] = "true";
         b3.mMinPlayersToMatch = 2;
         b3.mAlwaysAcceptJoinFlag = true;
         b3.sendSubs(1, 0);

         std::this_thread::sleep_for(std::chrono::seconds(1));
         b4.mGameSideInfos[1] = 1;
         b4.mGameSideInfos[2] = 2;
         b4.mGameOptions["op1"] = "false";
         b4.mMinPlayersToMatch = 2;
         b4.mAlwaysAcceptJoinFlag = true;
         b4.sendSubs(3, 10);

         std::this_thread::sleep_for(std::chrono::milliseconds(100));
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 2;
         mMinPlayersToMatch = 2;
         mAlwaysAcceptJoinFlag = true;
         sendSubs(3, 1);

      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(b2.mGotJoined);
         REQUIRE(b3.mGotJoined);
         REQUIRE(!(b4.mGotSubsResult));
         REQUIRE(mGotSubsResult);
         REQUIRE(mSessionID == b2.mSessionID);
         REQUIRE(b2.mSessionID == b3.mSessionID);
      }

      virtual void tearDown()
      {
         disconnect();
         b2.disconnect();
         b3.disconnect();
      }

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }

      Bot2 b2;
      Bot2 b3;
      Bot2 b4;
      bool mGotSubsResult;
   };

   GIVEN_DESCRIPTION(ExistingSessionVBetterMatch) = "A matched session through matchmaking and a non matching subscribe request";
   WHEN_DESCRIPTION(ExistingSessionVBetterMatch) = "another client sends a subscribe packet which can be match by both but the client is a better match";
   THEN_DESCRIPTION(ExistingSessionVBetterMatch) = "that request should be matched with the session";
   DEFINE_TEST(ExistingSessionVBetterMatch, "match_making", "priority in match making (existing session vs better match)")
}