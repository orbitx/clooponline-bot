#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace SubscribePackets
{

   class SubscribePackets : public Bot, public ITester<SubscribePackets>
   {
   public:
      SubscribePackets()
      {
         mGotException = mGotTID = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         login();
         connectToGameServer();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         sendSubs();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotTID);
         disconnect();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         reconnect();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendUSubs();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotException);
         sendSubs();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mGotException = false;
         sendUSubs();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotException);
      }

      virtual void tearDown()
      {
         disconnect();
      }

      virtual void subsDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::subsDone(server, reqPacket, repPacket);
         mGotTID = true;
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)
         if (innerpack.errorCode == ExceptionCode::INVALID_TID)
            mGotException = true;
      }


      bool mGotTID;
      bool mGotException;
   };

   GIVEN_DESCRIPTION(SubscribePackets) = "A clients that is connected to server";
   WHEN_DESCRIPTION(SubscribePackets) = "he sends a subscribe packet";
   THEN_DESCRIPTION(SubscribePackets) = "it should be successful and he should receive a track id";
   DEFINE_TEST(SubscribePackets, "match_making", "subscribe packet")

}