#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace MMEqualIndicator
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSubsResult = false;
      }

      bool mGotSubsResult;

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }
   };

   class MMEqualIndicator : public Bot, public ITester<MMEqualIndicator>
   {
   public:
      MMEqualIndicator()
      {
         mGotSubsResult = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         b3.initialize("valid_configs.json", "bot3");
         login();
         b2.login();
         b3.login();
         connectToGameServer();
         b2.connectToGameServer();
         b3.connectToGameServer();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mGameOptions["op1"] = "true";
         sendSubs(1, 10);

         std::this_thread::sleep_for(std::chrono::milliseconds(100));
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.mGameOptions["op1"] = "false";
         b2.sendSubs(1, 10);

         b3.mGameSideInfos[1] = 1;
         b3.mGameSideInfos[2] = 1;
         b3.sendSubs(2, 1);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotSubsResult);
         REQUIRE(b3.mGotSubsResult);
         REQUIRE(!(b2.mGotSubsResult));
         REQUIRE(mSessionID == b3.mSessionID);
      }

      virtual void tearDown()
      {
         disconnect();
         b2.disconnect();
         b3.disconnect();
      }

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }

      Bot2 b2;
      Bot2 b3;
      bool mGotSubsResult;
   };

   GIVEN_DESCRIPTION(MMEqualIndicator) = "two not matching clients in subscription pool, both having indicator 1";
   WHEN_DESCRIPTION(MMEqualIndicator) = "another client sends a subscribe packet which can be matched with both of them";
   THEN_DESCRIPTION(MMEqualIndicator) = "that client should be matched with the client who subscribed sooner than the other";
   DEFINE_TEST(MMEqualIndicator, "match_making", "priority in match making (equal indicator)")
}