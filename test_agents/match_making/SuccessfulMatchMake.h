#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace SuccessfulMatchMake
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSubsResult = false;
      }

      bool mGotSubsResult;

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }
   };

   class SuccessfulMatchMake : public Bot, public ITester<SuccessfulMatchMake>
   {
   public:
      SuccessfulMatchMake()
      {
         mGotSubsResult = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mGameOptions["a"] = "1";
         mGameOptions["b"] = "2";
         sendSubs(1, 1);

         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.mGameOptions["b"] = "2";
         b2.mGameOptions["c"] = "3";
         b2.sendSubs(1, 1);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotSubsResult);
         REQUIRE(b2.mGotSubsResult);
         REQUIRE(mSessionID == b2.mSessionID);
      }

      virtual void tearDown()
      {
         Bot::disconnect();
         b2.disconnect();
      }

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }

      Bot2 b2;
      bool mGotSubsResult;
   };

   GIVEN_DESCRIPTION(SuccessfulMatchMake) = "two clients that both sent a subscribe packet with matching options {“a”: 1, “b”: 2} and {“b”: 2, “c”: 3}";
   WHEN_DESCRIPTION(SuccessfulMatchMake) = "their poolTime runs out";
   THEN_DESCRIPTION(SuccessfulMatchMake) = "they should be matched and be joined in  a session";
   DEFINE_TEST(SuccessfulMatchMake, "match_making", "successful match make")

}