#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace MMTimeoutVBetterMatch
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSubsResult = false;
      }

      bool mGotSubsResult;

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }
   };

   class MMTimeoutVBetterMatch : public Bot, public ITester<MMTimeoutVBetterMatch>
   {
   public:
      MMTimeoutVBetterMatch()
      {
         mGotSubsResult = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         b3.initialize("valid_configs.json", "bot3");
         login();
         b2.login();
         b3.login();
         connectToGameServer();
         b2.connectToGameServer();
         b3.connectToGameServer();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mGameOptions["op1"] = "true";
         sendSubs(1, 0);

         std::this_thread::sleep_for(std::chrono::milliseconds(100));
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.mGameOptions["op1"] = "false";
         b2.sendSubs(2, 10);

         b3.mGameSideInfos[1] = 1;
         b3.mGameSideInfos[2] = 1;
         b3.sendSubs(3, 1);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotSubsResult);
         REQUIRE(b3.mGotSubsResult);
         REQUIRE(!(b2.mGotSubsResult));
         REQUIRE(mSessionID == b3.mSessionID);
      }

      virtual void tearDown()
      {
         disconnect();
         b2.disconnect();
         b3.disconnect();
      }

      virtual void gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotSubsResult(server, reqPacket, repPacket);
         mGotSubsResult = true;
      }

      Bot2 b2;
      Bot2 b3;
      bool mGotSubsResult;
   };

   GIVEN_DESCRIPTION(MMTimeoutVBetterMatch) = "two not matching clients in subscription pool, with indicators 1 and 2 but indicator 1 has been timed out in the pool";
   WHEN_DESCRIPTION(MMTimeoutVBetterMatch) = "another client sends a subscribe packet with indicator 3 which can be matched with both of them";
   THEN_DESCRIPTION(MMTimeoutVBetterMatch) = "indicator 1 and 3 should be matched";
   DEFINE_TEST(MMTimeoutVBetterMatch, "match_making", "priority in match making (timeout vs better match)")
}