#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ChangeSideBeforeStart
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSideChanged = mGotException = false;
         mGotUID = mGotNewSide = 0;
      }

      bool mGotException;
      bool mGotSideChanged;

      uint64_t mGotUID;
      uint     mGotNewSide;

      virtual void gotSideChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotSideChanged = true;
         HANDLE_METHOD_HEAD(SideChanged)
         mGotUID = uint64_t(innerpack.UID);
         mGotNewSide = uint(innerpack.newSide);
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotException = true;
      }
   };

   class ChangeSideBeforeStart : public Bot, public ITester<ChangeSideBeforeStart>
   {
   public:
      ChangeSideBeforeStart()
      {
         mGotSideChanged = mGotException = false;
         mGotUID = mGotNewSide = 0;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mSide = 1;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mSide = 2;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendChangeSide(0);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotException);
         REQUIRE(b2.mGotSideChanged);
         REQUIRE(b2.mGotUID == mUID);
         REQUIRE(b2.mGotNewSide == 0);
         mGotException = b2.mGotSideChanged = false;
         b2.mGotUID = b2.mGotNewSide = 0;
         b2.sendChangeSide(1);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!(b2.mGotException));
         REQUIRE(mGotSideChanged);
         REQUIRE(mGotUID == b2.mUID);
         REQUIRE(mGotNewSide == 1);
         b2.mGotException = mGotSideChanged = false;
         mGotUID = mGotNewSide = 0;
         sendChangeSide(1);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotException);
         REQUIRE(!(b2.mGotSideChanged));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::SIDE_NOT_AVAILABLE)
            mGotException = true;
      }

      virtual void gotSideChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotSideChanged = true;
         HANDLE_METHOD_HEAD(SideChanged)
         mGotUID = uint64_t(innerpack.UID);
         mGotNewSide = uint(innerpack.newSide);
      }

      Bot2 b2;
      bool mGotException;
      bool mGotSideChanged;
      uint64_t mGotUID;
      uint     mGotNewSide;
   };

   GIVEN_DESCRIPTION(ChangeSideBeforeStart) = "two clients in a same session which is not started yet";
   WHEN_DESCRIPTION(ChangeSideBeforeStart) = "one of them tries to change his side to either 0 or any available or any unavailable one";
   THEN_DESCRIPTION(ChangeSideBeforeStart) = "it should be successful, successful and unsuccessful accordingly and any accepted change should be informed to other clients";
   DEFINE_TEST(ChangeSideBeforeStart, "side", "changing side before start")
}