#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ManagerChangeCapMakeDC
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mIsInformationCorrect = mGotConfigChanged = false;
      }

      bool mGotConfigChanged;
      bool mIsInformationCorrect;

      virtual void gotConfigChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotConfigChanged = true;
         HANDLE_METHOD_HEAD(ConfigsChanged)
         if (innerpack.configs.sideToCapacity[1] == 1 &&
             innerpack.configs.sideToCapacity[2] == 1 &&
             innerpack.configs.participantsToSide[mUID] == 0)
            mIsInformationCorrect = true;
      }
   };

   class ManagerChangeCapMakeDC : public Bot, public ITester<ManagerChangeCapMakeDC>
   {
   public:
      ManagerChangeCapMakeDC()
      {
         mGotConfigChanged = mIsInformationCorrect = mGotException = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 2;
         mSide = 1;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mSide = 1;
         b2.sendJoinSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_sideToCapacity(std::map<int, int>({{1, 1}, {2, 1}}));
         sc.__set_participantsToSide(std::map<int64_t, int>({{mUID, 1}}));
         sendChangeConfig(sc);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotException);
         REQUIRE(mGotConfigChanged);
         REQUIRE(mIsInformationCorrect);
         REQUIRE(b2.mGotConfigChanged);
         REQUIRE(b2.mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotConfigChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotConfigChanged = true;
         HANDLE_METHOD_HEAD(ConfigsChanged)
         if (innerpack.configs.sideToCapacity[1] == 1 &&
             innerpack.configs.sideToCapacity[2] == 1 &&
             innerpack.configs.participantsToSide[mUID] == mSide)
            mIsInformationCorrect = true;
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)
         mGotException = true;
      }

      Bot2 b2;
      bool mGotException;
      bool mGotConfigChanged;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(ManagerChangeCapMakeDC) = "two clients in a same session";
   WHEN_DESCRIPTION(ManagerChangeCapMakeDC) = "the manager changes side capacities which does result in an exceeded capacity situation but there are open slots in other sides";
   THEN_DESCRIPTION(ManagerChangeCapMakeDC) = "it should be successful and no one should be kicked out of the session but some clients’ sides should become dont care";
   DEFINE_TEST(ManagerChangeCapMakeDC, "side", "manager changing side capacities make dc")
}