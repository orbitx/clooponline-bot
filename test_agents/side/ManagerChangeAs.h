#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ManagerChangeAs
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSideChanged = false;
         mGotUID = mGotNewSide = 0;
      }

      bool mGotSideChanged;

      uint64_t mGotUID;
      uint     mGotNewSide;

      virtual void gotSideChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotSideChanged = true;
         HANDLE_METHOD_HEAD(SideChanged)
         mGotUID = uint64_t(innerpack.UID);
         mGotNewSide = uint(innerpack.newSide);
      }
   };

   class ManagerChangeAs : public Bot, public ITester<ManagerChangeAs>
   {
   public:
      ManagerChangeAs()
      {
         mGotException = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mSide = 1;
         mCanChangeSideAfterStartFlag = false;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mSide = 2;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendChangeSide(0, b2.mUID);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotException);
         REQUIRE(b2.mGotSideChanged);
         REQUIRE(b2.mGotUID == b2.mUID);
         REQUIRE(b2.mGotNewSide == 0);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::INVALID_SESSION_STATE)
            mGotException = true;
      }

      Bot2 b2;
      bool mGotException;
   };

   GIVEN_DESCRIPTION(ManagerChangeAs) = "two clients in a same session";
   WHEN_DESCRIPTION(ManagerChangeAs) = "the manager changes side as the other client";
   THEN_DESCRIPTION(ManagerChangeAs) = "it should be successful regardless of session’s state and CCSAS flag";
   DEFINE_TEST(ManagerChangeAs, "side", "manager changing side as other clients")
}