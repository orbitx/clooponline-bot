#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace NotDeterminedAvailableSideJDS
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotJoinSessionResponse = mGotException = false;
      }

      bool mGotJoinSessionResponse;
      bool mGotException;

      virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::joinSessionDone(server, reqPacket, repPacket);
         mGotJoinSessionResponse = true;
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::WRONG_OPTIONS)
         {
            mGotException = true;
         }
      }
   };

   class NotDeterminedAvailableSideJDS : public Bot, public ITester<NotDeterminedAvailableSideJDS>
   {
   public:
      NotDeterminedAvailableSideJDS()
      {
         mGotJoined = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mSide = 0;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mSide = 0;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotJoined);
         REQUIRE(b2.mGotJoinSessionResponse);
         REQUIRE(!(b2.mGotException));
         REQUIRE(((mSide == 1 && b2.mSide == 2) || (mSide == 2 && b2.mSide == 1)));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotJoined(server, reqPacket, repPacket);
         mGotJoined = true;
      }

      Bot2 b2;
      bool mGotJoined;

   };

   GIVEN_DESCRIPTION(NotDeterminedAvailableSideJDS) = "a client who created a session with two sides each with capacity as one and that client himself took side 0";
   WHEN_DESCRIPTION(NotDeterminedAvailableSideJDS) = "other clients try to join to side 0";
   THEN_DESCRIPTION(NotDeterminedAvailableSideJDS) = "it should be successful and when the session starts the sides should be determined right";
   DEFINE_TEST(NotDeterminedAvailableSideJDS, "side", "not determined available side (join don't care side)")
}