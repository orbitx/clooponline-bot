#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ManagerChangeCapWithKickError
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotConfigChanged = mGotKicked = false;
      }

      bool mGotKicked;
      bool mGotConfigChanged;

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Left)
         mGotKicked = true;
      }

      virtual void gotConfigChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotConfigChanged = true;
      }
   };

   class ManagerChangeCapWithKickError : public Bot, public ITester<ManagerChangeCapWithKickError>
   {
   public:
      ManagerChangeCapWithKickError()
      {
         mGotException = mGotConfigChanged = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 2;
         mGameSideInfos[2] = 1;
         mSide = 1;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mSide = 1;
         b2.sendJoinSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_sideToCapacity(std::map<int, int>({{1, 1}}));
         sc.__set_participantsToSide(std::map<int64_t, int>({{mUID, 1}, {b2.mUID, 1}}));
         sendChangeConfig(sc);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotException);
         REQUIRE(!mGotConfigChanged);
         REQUIRE(!(b2.mGotKicked));
         REQUIRE(!(b2.mGotConfigChanged));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotConfigChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotConfigChanged = true;
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)
         if (innerpack.errorCode == ExceptionCode::INVALID_RESTRICTIONS)
            mGotException = true;
      }

      Bot2 b2;
      bool mGotException;
      bool mGotConfigChanged;
   };

   GIVEN_DESCRIPTION(ManagerChangeCapWithKickError) = "two clients in a same session";
   WHEN_DESCRIPTION(ManagerChangeCapWithKickError) = "the manager changes side capacities which does result in an exceeded capacity situation but states that all the clients should not be kicked";
   THEN_DESCRIPTION(ManagerChangeCapWithKickError) = "it should not be successful and manager should get an exception";
   DEFINE_TEST(ManagerChangeCapWithKickError, "side", "manager changing side capacities with kick error")
}