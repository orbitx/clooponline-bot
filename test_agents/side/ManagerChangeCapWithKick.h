#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ManagerChangeCapWithKick
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mIsInformationCorrect = mGotKicked = false;
      }

      bool mGotKicked;
      bool mIsInformationCorrect;

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Left)
         if (innerpack.UID == mUID)
            mGotKicked = true;

         if (innerpack.reason == LeavingReason::KICK)
            mIsInformationCorrect = true;
      }
   };

   class ManagerChangeCapWithKick : public Bot, public ITester<ManagerChangeCapWithKick>
   {
   public:
      ManagerChangeCapWithKick()
      {
         mGotConfigChanged = mIsInformationCorrect = mGotException = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 2;
         mGameSideInfos[2] = 1;
         mSide = 1;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.mSide = 1;
         b2.sendJoinSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_sideToCapacity(std::map<int, int>({{1, 1}}));
         sc.__set_participantsToSide(std::map<int64_t, int>({{mUID, 1}}));
         sendChangeConfig(sc);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotException);
         REQUIRE(mGotConfigChanged);
         REQUIRE(mIsInformationCorrect);
         REQUIRE(b2.mGotKicked);
         REQUIRE(b2.mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotConfigChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotConfigChanged = true;
         HANDLE_METHOD_HEAD(ConfigsChanged)
         if (innerpack.configs.sideToCapacity[1] == 1 &&
             innerpack.configs.sideToCapacity[2] == 0 &&
             innerpack.configs.participantsToSide[mUID] == mSide)
            mIsInformationCorrect = true;
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)
         mGotException = true;
      }

      Bot2 b2;
      bool mGotException;
      bool mGotConfigChanged;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(ManagerChangeCapWithKick) = "two clients in a same session";
   WHEN_DESCRIPTION(ManagerChangeCapWithKick) = "the manager changes side capacities which does result in an exceeded capacity situation and does not restrict the kicking";
   THEN_DESCRIPTION(ManagerChangeCapWithKick) = "it should be successful and no one should be kicked out of the session";
   DEFINE_TEST(ManagerChangeCapWithKick, "side", "manager changing side capacities with kick")
}