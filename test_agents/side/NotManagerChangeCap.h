#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace NotManagerChangeCap
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotConfigChanged = false;
      }

      bool mGotConfigChanged;

      virtual void gotConfigChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotConfigChanged = true;
         HANDLE_METHOD_HEAD(ConfigsChanged)
      }
   };

   class NotManagerChangeCap : public Bot, public ITester<NotManagerChangeCap>
   {
   public:
      NotManagerChangeCap()
      {
         mGotException = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.mSide = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         mSide = 2;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         SessionConfigs sc;
         sc.__set_sideToCapacity(std::map<int, int>({{1, 2}, {2, 2}}));
         sendChangeConfig(sc);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotException);
         REQUIRE(!(b2.mGotConfigChanged));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::NOT_MANAGER)
            mGotException = true;
      }

      Bot2 b2;
      bool mGotException;
   };

   GIVEN_DESCRIPTION(NotManagerChangeCap) = "two clients in a same session";
   WHEN_DESCRIPTION(NotManagerChangeCap) = "the client that is not manager changes side capacities";
   THEN_DESCRIPTION(NotManagerChangeCap) = "it should not be successful and an exception should be raised";
   DEFINE_TEST(NotManagerChangeCap, "side", "not manager changing side capacities")
}