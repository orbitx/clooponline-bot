#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace TrueCCSAS
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSideChanged = false;
         mGotUID = mGotNewSide = 0;
      }

      bool mGotSideChanged;

      uint64_t mGotUID;
      uint     mGotNewSide;

      virtual void gotSideChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotSideChanged = true;
         HANDLE_METHOD_HEAD(SideChanged)
         mGotUID = uint64_t(innerpack.UID);
         mGotNewSide = uint(innerpack.newSide);
      }
   };

   class TrueCCSAS : public Bot, public ITester<TrueCCSAS>
   {
   public:
      TrueCCSAS()
      {
         mGotException = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.mSide = 1;
         b2.mCanChangeSideAfterStartFlag = true;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         mSide = 2;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendChangeSide(0);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotException);
         REQUIRE(b2.mGotSideChanged);
         REQUIRE(b2.mGotUID == mUID);
         REQUIRE(b2.mGotNewSide == 0);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::INVALID_SESSION_STATE)
            mGotException = true;
      }

      Bot2 b2;
      bool mGotException;
   };

   GIVEN_DESCRIPTION(TrueCCSAS) = "two clients in a same session which is started and can change start after start is set to be true";
   WHEN_DESCRIPTION(TrueCCSAS) = "one of them tries to change his side to an available one";
   THEN_DESCRIPTION(TrueCCSAS) = "it should be successful any this change should be informed to other clients";
   DEFINE_TEST(TrueCCSAS, "side", "changing side after start with CCSAS as true")
}