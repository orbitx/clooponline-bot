#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace NotManagerChangeAs
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSideChanged = false;
      }

      bool mGotSideChanged;

      virtual void gotSideChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotSideChanged = true;
         HANDLE_METHOD_HEAD(SideChanged)
      }
   };

   class NotManagerChangeAs : public Bot, public ITester<NotManagerChangeAs>
   {
   public:
      NotManagerChangeAs()
      {
         mGotException = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.mSide = 1;
         b2.mCanChangeSideAfterStartFlag = false;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         mSide = 2;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendChangeSide(0, b2.mUID);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotException);
         REQUIRE(!(b2.mGotSideChanged));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::NOT_MANAGER)
            mGotException = true;
      }

      Bot2 b2;
      bool mGotException;
   };

   GIVEN_DESCRIPTION(NotManagerChangeAs) = "two clients in a same session";
   WHEN_DESCRIPTION(NotManagerChangeAs) = "the client that is not manager attempts to change the side of the other one";
   THEN_DESCRIPTION(NotManagerChangeAs) = "it should not be successful and an exception should be raised";
   DEFINE_TEST(NotManagerChangeAs, "side", "not manager changing side as other clients")
}