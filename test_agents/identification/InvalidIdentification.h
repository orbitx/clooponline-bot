#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

class InvalidIdentification : public Bot, public ITester<InvalidIdentification>
{
public:
   InvalidIdentification()
   {
      mExceptionRaised = false;
   }

   virtual void given()
   {
      Bot::initialize("valid_configs.json", "bot1");
   }

   virtual void when()
   {
      Bot::connectToGameServer();
   }

   virtual void then()
   {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      REQUIRE(mExceptionRaised);
   }

   virtual void tearDown()
   {
      Bot::disconnect();
   }

   virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
   {
      HANDLE_METHOD_HEAD(Exception)
      if (innerpack.errorCode == ExceptionCode::WRONG_OTP)
         mExceptionRaised = true;
   }

   bool mExceptionRaised;
};

GIVEN_DESCRIPTION(InvalidIdentification) = "invalid credentials";
WHEN_DESCRIPTION(InvalidIdentification) = "presented to server";
THEN_DESCRIPTION(InvalidIdentification) = "identification should be unsuccessful";
DEFINE_TEST(InvalidIdentification, "identification", "invalid identification")