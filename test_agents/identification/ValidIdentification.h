#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

class ValidIdentification : public Bot, public ITester<ValidIdentification>
{
public:
   ValidIdentification()
   {
      mExceptionRaised = false;
   }

   virtual void given()
   {
      Bot::initialize("valid_configs.json", "bot1");
   }

   virtual void when()
   {
      Bot::login();
      Bot::connectToGameServer();
   }

   virtual void then()
   {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      REQUIRE(!mExceptionRaised);
   }

   virtual void tearDown()
   {
      Bot::disconnect();
   }

   virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
   {
      HANDLE_METHOD_HEAD(Exception)
      mExceptionRaised = true;
   }

   bool mExceptionRaised;
};

GIVEN_DESCRIPTION(ValidIdentification) = "valid credentials";
WHEN_DESCRIPTION(ValidIdentification) = "presented to server";
THEN_DESCRIPTION(ValidIdentification) = "identification should be successful";
DEFINE_TEST(ValidIdentification, "identification", "valid identification")