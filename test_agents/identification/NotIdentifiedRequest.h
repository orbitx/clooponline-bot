#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

class NotIdentifiedRequest : public Bot, public ITester<NotIdentifiedRequest>
{
public:
   NotIdentifiedRequest()
   {
      mExceptionRaised = false;
   }

   virtual void given()
   {
      Bot::initialize("valid_configs.json", "bot1");
   }

   virtual void when()
   {
      Bot::connectToGameServer();
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      auto create = (ThriftParser*)createPacket();
      create->setMessageType(PacketType::CREATE_SESSION);
      fillSessionConfigs(create->pCreateSession.configs);
      create->pCreateSession.__set_side(mSide);
      sendRequest(mServer, create, bind(&NotIdentifiedRequest::createSessionDone, this, _1, _2, _3),
                  [this](ProfilePtr server2, PacketParser* reqPacket, PacketParser* repPacket){
                     HANDLE_METHOD_HEAD(Exception)
                     if (innerpack.errorCode == ExceptionCode::NOT_IDENTIFIED)
                        mExceptionRaised = true;
                  });
   }

   virtual void then()
   {
      std::this_thread::sleep_for(std::chrono::seconds(1));
      REQUIRE(mExceptionRaised);
   }

   virtual void tearDown()
   {
      Bot::disconnect();
   }

   bool mExceptionRaised;
};

GIVEN_DESCRIPTION(NotIdentifiedRequest) = "a client and a connection to server";
WHEN_DESCRIPTION(NotIdentifiedRequest) = "this client sends a request before it has been identified";
THEN_DESCRIPTION(NotIdentifiedRequest) = "it should receive a not identified exception";
DEFINE_TEST(NotIdentifiedRequest, "identification", "requests from not identified clients")