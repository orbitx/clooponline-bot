#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ExcludedFilterSessionList
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSessions = false;
      }

      bool mGotSessions;

      virtual void sessionListDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::sessionListDone(server, reqPacket, repPacket);
         mGotSessions = true;
      }
   };

   class ExcludedFilterSessionList : public Bot, public ITester<ExcludedFilterSessionList>
   {
   public:
      ExcludedFilterSessionList()
      {
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameOptions["a"] = "1";
         mGameOptions["b"] = "2";
         Bot::sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mGameOptions["a"] = "1";
         b2.mGameOptions["b"] = "3";
         b2.sendSessionList();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mGotSessions);
         REQUIRE(b2.mSessionsList.size() == 0);
      }

      virtual void tearDown()
      {
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
   };

   GIVEN_DESCRIPTION(ExcludedFilterSessionList) = "a client which has created a session with some set options";
   WHEN_DESCRIPTION(ExcludedFilterSessionList) = "another client requests the session list with filter that does not pass that session";
   THEN_DESCRIPTION(ExcludedFilterSessionList) = "it should not receive the created session in that list";
   DEFINE_TEST(ExcludedFilterSessionList, "session_list", "excluded filtered session list")

}