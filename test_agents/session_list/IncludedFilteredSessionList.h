#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace IncludedFilterSessionList
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSessions = false;
      }

      bool mGotSessions;

      virtual void sessionListDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::sessionListDone(server, reqPacket, repPacket);
         mGotSessions = true;
      }
   };

   class IncludedFilterSessionList : public Bot, public ITester<IncludedFilterSessionList>
   {
   public:
      IncludedFilterSessionList()
      {
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameOptions["a"] = "1";
         mGameOptions["b"] = "2";
         Bot::sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mGameOptions["a"] = "1";
         b2.sendSessionList();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mGotSessions);
         REQUIRE(b2.mSessionsList.size() == 1);
         REQUIRE(b2.mSessionsList.begin()->first == mSessionID);
      }

      virtual void tearDown()
      {
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
   };

   GIVEN_DESCRIPTION(IncludedFilterSessionList) = "a client which has created a session with some set options";
   WHEN_DESCRIPTION(IncludedFilterSessionList) = "another client requests the session list with a filter that passes that session";
   THEN_DESCRIPTION(IncludedFilterSessionList) = "it should receive the created session in that list";
   DEFINE_TEST(IncludedFilterSessionList, "session_list", "included filtered session list")

}