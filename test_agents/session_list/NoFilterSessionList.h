#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace NoFilterSessionList
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotSessions = false;
      }

      bool mGotSessions;

      virtual void sessionListDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::sessionListDone(server, reqPacket, repPacket);
         mGotSessions = true;
      }
   };

   class NoFilterSessionList : public Bot, public ITester<NoFilterSessionList>
   {
   public:
      NoFilterSessionList()
      {
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 2;
         mSide = 1;
         mGameOptions["a"] = "1";
         mGameOptions["b"] = "2";
         mAlwaysAcceptJoinFlag = mCanChangeSideAfterStartFlag = true;
         mDisconnectionAction = DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT;
         mDisconnectionTimeout = 10;
         mSessionLabel = "test_session";
         Bot::sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::sendRPC(0, true);
         Bot::sendSetProperty("modify", "a", 1, 0);
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendSessionList();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mGotSessions);
         REQUIRE(b2.mSessionsList.size() == 1);
         REQUIRE(b2.mSessionsList.begin()->first == mSessionID);
         REQUIRE(b2.mSessionsList.begin()->second.options == mGameOptions);
         REQUIRE(b2.mSessionsList.begin()->second.lastRPCIndex == 1);
         REQUIRE(b2.mSessionsList.begin()->second.propertyRevisionIndex == 1);
         REQUIRE(b2.mSessionsList.begin()->second.alwaysAcceptJoin);
         REQUIRE(b2.mSessionsList.begin()->second.canChangeSideAfterStart);
         REQUIRE(b2.mSessionsList.begin()->second.disconnectionBehavior == DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT);
         REQUIRE(b2.mSessionsList.begin()->second.disconnectionTimeout == 10);
         REQUIRE(b2.mSessionsList.begin()->second.label == "test_session");
      }

      virtual void tearDown()
      {
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
   };

   GIVEN_DESCRIPTION(NoFilterSessionList) = "a client which has created a session with some set options";
   WHEN_DESCRIPTION(NoFilterSessionList) = "another client requests the session list with no filter";
   THEN_DESCRIPTION(NoFilterSessionList) = "it should receive the created session in that list";
   DEFINE_TEST(NoFilterSessionList, "session_list", "no filter session list")

}