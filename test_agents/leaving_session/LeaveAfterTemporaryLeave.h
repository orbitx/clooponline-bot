#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace LeaveAfterTempLeave
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotRejoined = false;
         mGotLeft = false;
      }

      bool mGotRejoined;
      bool mGotLeft;

      virtual void gotRejoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotRejoined = true;
         Bot::gotRejoined(server, reqPacket, repPacket);
      }

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotLeft = true;
         Bot::gotLeft(server, reqPacket, repPacket);
      }
   };

   class LeaveAfterTempLeave : public Bot, public ITester<LeaveAfterTempLeave>
   {
   public:
      LeaveAfterTempLeave()
      {
         mGotRPC = mGotProperties = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mAllShouldBeReadyFlag = false;
         b2.mStartTimeout = 0;
         b2.sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendRPC(0, true);
         sendSetProperty("modify", "a", 1, 0);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendTempLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendHandleActiveSession(mSessionID, "leave");
      }

      virtual void handleActiveSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(HandleActiveSessionResponse)
         mGotRPC = (innerpack.joinObject.cachedRPCs.size() == 1);
         mGotProperties = (innerpack.joinObject.properties.children.find("a") != innerpack.joinObject.properties.children.end());
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!(b2.mGotRejoined));
         REQUIRE(b2.mGotLeft);
         REQUIRE(!mGotRPC);
         REQUIRE(!mGotProperties);
         sendGetActiveSessions();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mAvailableActiveSessions.size() == 0);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
      bool mGotRPC;
      bool mGotProperties;
   };

   GIVEN_DESCRIPTION(LeaveAfterTempLeave) = "two clients in a same session";
   WHEN_DESCRIPTION(LeaveAfterTempLeave) = "one of them temporarily leaves that session then leaves that session permanently";
   THEN_DESCRIPTION(LeaveAfterTempLeave) = "it should be successful and that session should not be in the left client’s active session list anymore and the other client should be informed about this";
   DEFINE_TEST(LeaveAfterTempLeave, "leaving_session", "leave after temporary leave")
}