#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace TemporaryLeave
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotTempLeft = false;
      }

      bool mGotTempLeft;

      virtual void gotTempLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotTempLeft = true;
         Bot::gotTempLeft(server, reqPacket, repPacket);
      }
   };

   class TemporaryLeave : public Bot, public ITester<TemporaryLeave>
   {
   public:
      TemporaryLeave()
      {
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mAllShouldBeReadyFlag = false;
         b2.mStartTimeout = 0;
         b2.sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendTempLeave();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mGotTempLeft);
         sendGetActiveSessions();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mAvailableActiveSessions.size() == 1);
         REQUIRE(mAvailableActiveSessions[0] == b2.mSessionID);
      }

      virtual void tearDown()
      {
         sendHandleActiveSession(mSessionID, "leave");
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
   };

   GIVEN_DESCRIPTION(TemporaryLeave) = "two clients in a same session";
   WHEN_DESCRIPTION(TemporaryLeave) = "one of them temporarily leaves that session";
   THEN_DESCRIPTION(TemporaryLeave) = "it should be successful and that session should be in the left client’s active session list";
   DEFINE_TEST(TemporaryLeave, "leaving_session", "temporary leave")
}