#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace PermanentLeave
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotLeft = false;
      }

      bool mGotLeft;

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotLeft = true;
         Bot::gotLeft(server, reqPacket, repPacket);
      }
   };

   class PermanentLeave : public Bot, public ITester<PermanentLeave>
   {
   public:
      PermanentLeave()
      {
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mAllShouldBeReadyFlag = false;
         b2.mStartTimeout = 0;
         b2.sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendLeave();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mGotLeft);
         sendGetActiveSessions();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mAvailableActiveSessions.size() == 0);
      }

      virtual void tearDown()
      {
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;

   };

   GIVEN_DESCRIPTION(PermanentLeave) = "two clients in a same session";
   WHEN_DESCRIPTION(PermanentLeave) = "one of them permanently leaves that session";
   THEN_DESCRIPTION(PermanentLeave) = "it should be successful and the other client should be informed about the one that left and that session should not be in the left client’s active session list";
   DEFINE_TEST(PermanentLeave, "leaving_session", "permanent leave")
}