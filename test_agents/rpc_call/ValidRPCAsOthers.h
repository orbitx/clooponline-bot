#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ValidRPCAsOthers
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotRPC = false;
      }

      bool mGotRPC;

      virtual void gotRPC(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotRPC = true;
         Bot::gotRPC(server, reqPacket, repPacket);
      }
   };

   class ValidRPCAsOthers : public Bot, public ITester<ValidRPCAsOthers>
   {
   public:
      ValidRPCAsOthers()
      {
         mIsUIDCorrect = mGotRPC = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         b3.initialize("valid_configs.json", "bot3");
         Bot::login();
         b2.login();
         b3.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b3.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 2;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b3.mSessionID = mSessionID = b2.mSessionID;
         sendJoinSession();
         b3.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendRPC(b3.mUID, false);
      }

      virtual void gotRPC(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(RPCCalled)
         Bot::gotRPC(server, reqPacket, repPacket);
         mGotRPC = true;
         if (innerpack.UID == b3.mUID)
            mIsUIDCorrect = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!(b2.mGotRPC));
         REQUIRE(b3.mGotRPC);
         REQUIRE(mGotRPC);
         REQUIRE(mIsUIDCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         b3.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
         b3.disconnect();
      }

      Bot2 b2;
      Bot2 b3;
      bool mGotRPC;
      bool mIsUIDCorrect;
   };

   GIVEN_DESCRIPTION(ValidRPCAsOthers) = "three clients in a same session";
   WHEN_DESCRIPTION(ValidRPCAsOthers) = "manager sends an RPC packet as one of the other clients";
   THEN_DESCRIPTION(ValidRPCAsOthers) = "other clients should get an RPC as if that client sent it";
   DEFINE_TEST(ValidRPCAsOthers, "rpc_call", "valid rpc call as others")
}