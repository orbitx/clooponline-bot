#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace RPCWithInvalidReceptors
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotRPC = false;
      }

      bool mGotRPC;

      virtual void gotRPC(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotRPC = true;
         Bot::gotRPC(server, reqPacket, repPacket);
      }
   };

   class RPCWithInvalidReceptors : public Bot, public ITester<RPCWithInvalidReceptors>
   {
   public:
      RPCWithInvalidReceptors()
      {
         mGotException = mGotRPC = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         b3.initialize("valid_configs.json", "bot3");
         Bot::login();
         b2.login();
         b3.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b3.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 2;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b3.mSessionID = mSessionID = b2.mSessionID;
         sendJoinSession();
         b3.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendRPC({123123}, 0, false);
      }

      virtual void gotRPC(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotRPC = true;
         Bot::gotRPC(server, reqPacket, repPacket);
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)
         if (innerpack.errorCode == ExceptionCode::NOT_A_PARTICIPANT)
            mGotException = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!(b2.mGotRPC));
         REQUIRE(!(b3.mGotRPC));
         REQUIRE(!mGotRPC);
         REQUIRE(mGotException);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         b3.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
         b3.disconnect();
      }

      Bot2 b2;
      Bot2 b3;
      bool mGotRPC;
      bool mGotException;
   };

   GIVEN_DESCRIPTION(RPCWithInvalidReceptors) = "three clients in a same session";
   WHEN_DESCRIPTION(RPCWithInvalidReceptors) = "one of them sends an RPC packet with invalid receptors";
   THEN_DESCRIPTION(RPCWithInvalidReceptors) = "an exception should be raised";
   DEFINE_TEST(RPCWithInvalidReceptors, "rpc_call", "rpc call with invalid receptors")
}