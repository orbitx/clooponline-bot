#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace SimpleRPCCall
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotFirstRPC = false;
         mGotSecondRPC = false;
      }

      bool mGotFirstRPC;
      bool mGotSecondRPC;

      virtual void gotRPC(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         if (!mGotFirstRPC)
            mGotFirstRPC = true;
         else if (!mGotSecondRPC)
            mGotSecondRPC = true;
         Bot::gotRPC(server, reqPacket, repPacket);
      }
   };

   class SimpleRPCCall : public Bot, public ITester<SimpleRPCCall>
   {
   public:
      SimpleRPCCall()
      {
         mGotFirstRPC = mGotSecondRPC = mIsIndicesTandem = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendRPC(0, false);
         sendRPC(0, false);
      }

      virtual void RPCCallDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(RPCCallResponse)

         if (!mGotFirstRPC)
            mGotFirstRPC = true;
         else if (!mGotSecondRPC)
         {
            mGotSecondRPC = true;
            if (innerpack.RPCindex == mLastRPCIndex + 1)
            {
               mIsIndicesTandem = true;
            }
         }

         Bot::RPCCallDone(server, reqPacket, repPacket);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(b2.mGotFirstRPC);
         REQUIRE(b2.mGotSecondRPC);
         REQUIRE(mGotFirstRPC);
         REQUIRE(mGotSecondRPC);
         REQUIRE(mIsIndicesTandem);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      Bot2 b2;
      bool mGotFirstRPC;
      bool mGotSecondRPC;
      bool mIsIndicesTandem;
   };

   GIVEN_DESCRIPTION(SimpleRPCCall) = "two clients in a same session";
   WHEN_DESCRIPTION(SimpleRPCCall) = "one of them sends two RPC packets";
   THEN_DESCRIPTION(SimpleRPCCall) = "he should receive two tandem RPC Indices as response and the other client should be informed about these two RPCs";
   DEFINE_TEST(SimpleRPCCall, "rpc_call", "simple rpc call")
}