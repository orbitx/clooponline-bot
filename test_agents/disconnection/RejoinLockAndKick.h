#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace RejoinLockAndKick
{
   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotRejoined = mGotTempLeft = mGotLock = mGotResume = mGotTerminate = false;
      }

      bool mGotRejoined;
      bool mGotTempLeft;
      bool mGotLock;
      bool mGotResume;
      bool mGotTerminate;

      virtual void gotRejoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotRejoined = true;
         Bot::gotRejoined(server, reqPacket, repPacket);
      }

      virtual void gotTempLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotTempLeft = true;
         Bot::gotTempLeft(server, reqPacket, repPacket);
      }

      virtual void gotLock(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotLock(server, reqPacket, repPacket);
         mGotLock = true;
      }

      virtual void gotResume(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotResume(server, reqPacket, repPacket);
         mGotResume = true;
      }

      virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotTerminate(server, reqPacket, repPacket);
         mGotTerminate = true;
      }
   };

   class RejoinLockAndKick : public Bot, public ITester<RejoinLockAndKick>
   {
   public:
      RejoinLockAndKick()
      {
         mGotActiveSession = mIsInformationCorrect = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.mDisconnectionAction = DisconnectionActionType::LOCK_AND_KICK_ON_TIMEOUT;
         b2.mDisconnectionTimeout = 5;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         reconnect();
      }

      virtual void gotActiveSession(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotActiveSession(server, reqPacket, repPacket);

         mGotActiveSession = true;
         if (mAvailableActiveSessions.size() == 1 && mAvailableActiveSessions[0] == b2.mSessionID)
            mIsInformationCorrect = true;

         sendHandleActiveSession(mAvailableActiveSessions[0], "join");
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotActiveSession);
         REQUIRE(mIsInformationCorrect);
         REQUIRE(b2.mGotTempLeft);
         REQUIRE(b2.mGotRejoined);
         REQUIRE(b2.mGotLock);
         REQUIRE(b2.mGotResume);
         REQUIRE(!(b2.mGotTerminate));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
         b2.disconnect();
      }

      Bot2 b2;
      bool mGotActiveSession;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(RejoinLockAndKick) = "two clients in a same session with disconnection behavior as lock and kick on timeout";
   WHEN_DESCRIPTION(RejoinLockAndKick) = "one of them is disconnected then reconnects";
   THEN_DESCRIPTION(RejoinLockAndKick) = "the session should be locked automatically and should be resumed as soon as that client rejoins the session";
   DEFINE_TEST(RejoinLockAndKick, "disconnection", "rejoin lock and kick on timeout")
}