#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace DCTimeout
{
   class DCTimeout : public Bot, public ITester<DCTimeout>
   {
   public:
      DCTimeout()
      {
         mGotTerminate = mIsInformationCorrect = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mDisconnectionAction = DisconnectionActionType::TERMINATE;
         mDisconnectionTimeout = 1;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.disconnect();
      }

      virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotTerminate(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(Terminated)
         mGotTerminate = true;
         if (innerpack.reason == TerminationReason::DISCONNECTION)
            mIsInformationCorrect = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotTerminate);
         REQUIRE(mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      Bot  b2;
      bool mGotTerminate;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(DCTimeout) = "two clients in a same session with disconnection behavior as terminate";
   WHEN_DESCRIPTION(DCTimeout) = "one of them is disconnected";
   THEN_DESCRIPTION(DCTimeout) = "that session should be terminated automatically";
   DEFINE_TEST(DCTimeout, "disconnection", "disconnection behavior as terminate")
}