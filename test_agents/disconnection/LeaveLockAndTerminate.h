#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace LeaveLockAndTerminate
{

   class LeaveLockAndTerminate : public Bot, public ITester<LeaveLockAndTerminate>
   {
   public:
      LeaveLockAndTerminate()
      {
         mGotRejoined = mGotLeft = mGotTempLeft = mGotLock = mGotResume = mGotTerminate = mIsInformationCorrect = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mDisconnectionAction = DisconnectionActionType::LOCK_AND_TERMINATE_ON_TIMEOUT;
         mDisconnectionTimeout = 1;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.disconnect();
      }

      virtual void gotRejoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotRejoined = true;
         Bot::gotRejoined(server, reqPacket, repPacket);
      }

      virtual void gotTempLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotTempLeft = true;
         Bot::gotTempLeft(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(TemporaryLeft)
         if (innerpack.UID == b2.mUID)
            mIsInformationCorrect = true;
      }

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotLeft = true;
         Bot::gotTempLeft(server, reqPacket, repPacket);
      }

      virtual void gotLock(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotLock(server, reqPacket, repPacket);
         mGotLock = true;
      }

      virtual void gotResume(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotResume(server, reqPacket, repPacket);
         mGotResume = true;
      }

      virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotTerminate(server, reqPacket, repPacket);

         mGotTerminate = true;
         HANDLE_METHOD_HEAD(Terminated)
         if (innerpack.reason == TerminationReason::DISCONNECTION)
            mIsInformationCorrect = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotTempLeft);
         REQUIRE(mGotLock);
         REQUIRE(!mGotResume);
         REQUIRE(mGotTerminate);
         REQUIRE(!mGotLeft);
         REQUIRE(!mGotRejoined);
         REQUIRE(mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      Bot  b2;
      bool mGotRejoined;
      bool mGotTempLeft;
      bool mGotLeft;
      bool mGotLock;
      bool mGotResume;
      bool mGotTerminate;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(LeaveLockAndTerminate) = "two clients in a same session with disconnection behavior as lock and terminate on timeout";
   WHEN_DESCRIPTION(LeaveLockAndTerminate) = "one of them is disconnected";
   THEN_DESCRIPTION(LeaveLockAndTerminate) = "the session should be locked automatically and that that session should be terminated when timeout is reached";
   DEFINE_TEST(LeaveLockAndTerminate, "disconnection", "leave lock and terminate on timeout")
}