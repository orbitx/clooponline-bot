#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace NotManagerKick
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotLeft = false;
      }

      bool mGotLeft;

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotLeft = true;
         Bot::gotLeft(server, reqPacket, repPacket);
      }
   };

   class NotManagerKick : public Bot, public ITester<NotManagerKick>
   {
   public:
      NotManagerKick()
      {
         mGotException = mGotLeft = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         b2.mGameSideInfos[1] = 1;
         b2.mGameSideInfos[2] = 1;
         b2.sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSessionID = b2.mSessionID;
         sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendKick(b2.mUID);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotLeft);
         REQUIRE(mGotException);
         REQUIRE(!(b2.mGotLeft));
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotLeft = true;
         Bot::gotLeft(server, reqPacket, repPacket);
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotException = true;
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::NOT_MANAGER)
            mGotException = true;
      }

      Bot2 b2;
      bool mGotLeft;
      bool mGotException;
   };

   GIVEN_DESCRIPTION(NotManagerKick) = "two clients in a same session";
   WHEN_DESCRIPTION(NotManagerKick) = "the client that is not manager, kicks the other one";
   THEN_DESCRIPTION(NotManagerKick) = "it should not be successful and it should receive an exception";
   DEFINE_TEST(NotManagerKick, "disconnection", "not manager kick")
}
