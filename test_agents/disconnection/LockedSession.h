#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace LockedSession
{

   class LockedSession : public Bot, public ITester<LockedSession>
   {
   public:
      LockedSession()
      {
         mGotPRCException = mGotPropertiesException = mGotChangeConfigException = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mDisconnectionAction = DisconnectionActionType::LOCK_AND_KICK_ON_TIMEOUT;
         mDisconnectionTimeout = 0;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.disconnect();
      }

      virtual void when()
      {
         mSendingRPC = true;
         mSendingProperties = false;
         mSendingChangeConfig = false;
         sendRPC();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSendingRPC = false;
         mSendingProperties = true;
         mSendingChangeConfig = false;
         sendSetProperty("modify", "a", 123, 0);
         std::this_thread::sleep_for(std::chrono::seconds(1));
         mSendingRPC = false;
         mSendingProperties = false;
         mSendingChangeConfig = true;
         SessionConfigs sc;
         sc.__set_disconnectionBehavior(DisconnectionActionType::KICK);
         sendChangeConfig(sc);
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::INVALID_SESSION_STATE)
         {
            if (mSendingRPC)
               mGotPRCException = true;
            else if (mSendingProperties)
               mGotPropertiesException = true;
            else if (mSendingChangeConfig)
               mGotChangeConfigException = true;
         }
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotPRCException);
         REQUIRE(mGotPropertiesException);
         REQUIRE(mGotChangeConfigException);
      }

      virtual void tearDown()
      {
         sendTerminate();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      Bot  b2;
      bool mSendingRPC;
      bool mSendingProperties;
      bool mSendingChangeConfig;
      bool mGotPRCException;
      bool mGotPropertiesException;
      bool mGotChangeConfigException;
   };

   GIVEN_DESCRIPTION(LockedSession) = "two clients in a same session and situation that results in locking session";
   WHEN_DESCRIPTION(LockedSession) = "clients in that session try to send RPC or set Properties or change configs";
   THEN_DESCRIPTION(LockedSession) = "it should not be successful and it should receive an exception";
   DEFINE_TEST(LockedSession, "disconnection", "locked session")
}