#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace LeaveContinueAndKick
{

   class LeaveContinueAndKick : public Bot, public ITester<LeaveContinueAndKick>
   {
   public:
      LeaveContinueAndKick()
      {
         mGotLeft = mGotLock = mGotTerminate = mIsInformationCorrect = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mDisconnectionAction = DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT;
         mDisconnectionTimeout = 1;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.disconnect();
      }

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotLeft(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(Left)
         mGotLeft = true;
         if (innerpack.UID == b2.mUID && innerpack.reason == LeavingReason::DISCONNECTION)
            mIsInformationCorrect = true;
      }

      virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotTerminate(server, reqPacket, repPacket);
         mGotLeft = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(mGotLeft);
         REQUIRE(!mGotLock);
         REQUIRE(!mGotTerminate);
         REQUIRE(mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      Bot b2;
      bool mGotLeft;
      bool mGotLock;
      bool mGotTerminate;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(LeaveContinueAndKick) = "two clients in a same session with disconnection behavior as continue and kick on timeout";
   WHEN_DESCRIPTION(LeaveContinueAndKick) = "one of them is disconnected";
   THEN_DESCRIPTION(LeaveContinueAndKick) = "the session should not be locked and should kick that client when timeout is reached";
   DEFINE_TEST(LeaveContinueAndKick, "disconnection", "leave continue and kick on timeout")
}