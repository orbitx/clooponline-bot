#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ManagerKick
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotLeft = mIsInformationCorrect = false;
      }

      bool mGotLeft;
      bool mIsInformationCorrect;

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotLeft = true;
         Bot::gotLeft(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(Left)
         if (innerpack.UID == mUID && innerpack.reason == LeavingReason::KICK)
            mIsInformationCorrect = true;
      }
   };

   class ManagerKick : public Bot, public ITester<ManagerKick>
   {
   public:
      ManagerKick()
      {
         mGotLeft = mIsInformationCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendKick(b2.mUID);
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotLeft);
         REQUIRE(mIsInformationCorrect);
         REQUIRE(b2.mGotLeft);
         REQUIRE(b2.mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotLeft = true;
         Bot::gotLeft(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(Left)
         if (innerpack.UID == b2.mUID && innerpack.reason == LeavingReason::KICK)
            mIsInformationCorrect = true;
      }

      Bot2 b2;
      bool mGotLeft;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(ManagerKick) = "two clients in a same session";
   WHEN_DESCRIPTION(ManagerKick) = "manager kicks the other one";
   THEN_DESCRIPTION(ManagerKick) = "it should be successful";
   DEFINE_TEST(ManagerKick, "disconnection", "manager kick")
}
