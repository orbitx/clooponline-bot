#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace DCKick
{
   class DCKick : public Bot, public ITester<DCKick>
   {
   public:
      DCKick()
      {
         mGotLeft = mIsInformationCorrect = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mDisconnectionAction = DisconnectionActionType::KICK;
         mDisconnectionTimeout = 1;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.disconnect();
      }

      virtual void gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Left)
         mGotLeft = true;
         if (innerpack.UID == b2.mUID && innerpack.reason == LeavingReason::DISCONNECTION)
            mIsInformationCorrect = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotLeft);
         REQUIRE(mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      Bot  b2;
      bool mGotLeft;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(DCKick) = "two clients in a same session with disconnection behavior as kick";
   WHEN_DESCRIPTION(DCKick) = "one of them is disconnected";
   THEN_DESCRIPTION(DCKick) = "that client should be kicked automatically out of the session";
   DEFINE_TEST(DCKick, "disconnection", "disconnection behavior as kick")
}