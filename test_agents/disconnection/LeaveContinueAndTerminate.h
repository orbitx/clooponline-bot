#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace LeaveContinueAndTerminate
{

   class LeaveContinueAndTerminate : public Bot, public ITester<LeaveContinueAndTerminate>
   {
   public:
      LeaveContinueAndTerminate()
      {
         mGotLock = mGotTerminate = mIsInformationCorrect = false;
      }

      virtual void given()
      {
         initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         login();
         b2.login();
         connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         mDisconnectionAction = DisconnectionActionType::CONTINUE_AND_TERMINATE_ON_TIMEOUT;
         mDisconnectionTimeout = 1;
         sendCreateSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendStartSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.disconnect();
      }

      virtual void gotLock(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotLock(server, reqPacket, repPacket);
         mGotLock = true;
      }

      virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         Bot::gotTerminate(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(Terminated)
         mGotTerminate = true;
         if (innerpack.reason == TerminationReason::DISCONNECTION)
            mIsInformationCorrect = true;
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(2));
         REQUIRE(!mGotLock);
         REQUIRE(mGotTerminate);
         REQUIRE(mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         disconnect();
      }

      Bot b2;
      bool mGotLock;
      bool mGotTerminate;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(LeaveContinueAndTerminate) = "two clients in a same session with disconnection behavior as continue and terminate on timeout";
   WHEN_DESCRIPTION(LeaveContinueAndTerminate) = "one of them is disconnected";
   THEN_DESCRIPTION(LeaveContinueAndTerminate) = "the session should not be locked and the session should be terminated when timeout is reached";
   DEFINE_TEST(LeaveContinueAndTerminate, "disconnection", "leave continue and terminate on timeout")
}