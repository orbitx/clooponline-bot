#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace NotManagerTerminate
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotTerminate = mGotException = false;
      }

      bool mGotTerminate;
      bool mGotException;

      virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotTerminate = true;
         Bot::gotTerminate(server, reqPacket, repPacket);
      }

      virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         HANDLE_METHOD_HEAD(Exception)

         if (innerpack.errorCode == ExceptionCode::NOT_MANAGER)
            mGotException = true;
      }
   };

   class NotManagerTerminate : public Bot, public ITester<NotManagerTerminate>
   {
   public:
      NotManagerTerminate()
      {
         mGotTerminate = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.sendTerminate();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(!mGotTerminate);
         REQUIRE(!(b2.mGotTerminate));
         REQUIRE(b2.mGotException);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotTerminate = true;
         Bot::gotTerminate(server, reqPacket, repPacket);
      }

      Bot2 b2;
      bool mGotTerminate;
   };

   GIVEN_DESCRIPTION(NotManagerTerminate) = "two clients in a same session";
   WHEN_DESCRIPTION(NotManagerTerminate) = "the client that is not manager, terminates the session";
   THEN_DESCRIPTION(NotManagerTerminate) = "it should not be successful and it should receive an exception";
   DEFINE_TEST(NotManagerTerminate, "disconnection", "not manager terminate")
}
