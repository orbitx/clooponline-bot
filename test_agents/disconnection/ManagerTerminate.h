#pragma once

#include <Bot.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <ITester.h>

using namespace clooponline::packet;

namespace ManagerTerminate
{

   class Bot2 : public Bot
   {
   public:
      Bot2()
      {
         mGotTerminate = mIsInformationCorrect = false;
      }

      bool mGotTerminate;
      bool mIsInformationCorrect;

      virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotTerminate = true;
         Bot::gotTerminate(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(Terminated)
         if (innerpack.reason == TerminationReason::ON_DEMAND)
            mIsInformationCorrect = true;
      }
   };

   class ManagerTerminate : public Bot, public ITester<ManagerTerminate>
   {
   public:
      ManagerTerminate()
      {
         mGotTerminate = mIsInformationCorrect = false;
      }

      virtual void given()
      {
         Bot::initialize("valid_configs.json", "bot1");
         b2.initialize("valid_configs.json", "bot2");
         Bot::login();
         b2.login();
         Bot::connectToGameServer();
         b2.connectToGameServer();
         mGameSideInfos[1] = 1;
         mGameSideInfos[2] = 1;
         sendCreateSession();
      }

      virtual void when()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         b2.mSessionID = mSessionID;
         b2.sendJoinSession();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         sendTerminate();
      }

      virtual void then()
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
         REQUIRE(mGotTerminate);
         REQUIRE(mIsInformationCorrect);
         REQUIRE(b2.mGotTerminate);
         REQUIRE(b2.mIsInformationCorrect);
      }

      virtual void tearDown()
      {
         sendLeave();
         b2.sendLeave();
         std::this_thread::sleep_for(std::chrono::seconds(1));
         Bot::disconnect();
         b2.disconnect();
      }

      virtual void gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
      {
         mGotTerminate = true;
         Bot::gotTerminate(server, reqPacket, repPacket);

         HANDLE_METHOD_HEAD(Terminated)
         if (innerpack.reason == TerminationReason::ON_DEMAND)
            mIsInformationCorrect = true;
      }

      Bot2 b2;
      bool mGotTerminate;
      bool mIsInformationCorrect;
   };

   GIVEN_DESCRIPTION(ManagerTerminate) = "two clients in a same session";
   WHEN_DESCRIPTION(ManagerTerminate) = "manager terminates the session";
   THEN_DESCRIPTION(ManagerTerminate) = "it should be successful";
   DEFINE_TEST(ManagerTerminate, "disconnection", "manager terminate")
}
