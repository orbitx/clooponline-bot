cmake_minimum_required(VERSION 2.8)
project(clooponline-testbot)
SET(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS_RELEASE "-O2 -std=c++14 -Wno-deprecated-declarations")
set(CMAKE_CXX_FLAGS_DEBUG "-g -std=c++14 -Wno-deprecated-declarations")

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/build")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")

find_package(clooponline-nethandler REQUIRED)

include_directories("include"
					"test_agents"
					${clooponline-nethandler_INCLUDE_DIR})

add_library(clooponline-testbot
                    src/Bot.cpp
					include/Bot.h)

target_link_libraries(clooponline-testbot
		${clooponline-nethandler_LIBRARIES}
		cxxtools
		cxxtools-json
		)

add_executable(clooponline-clibot
					src/main.cpp)
target_link_libraries(clooponline-clibot clooponline-testbot)

add_executable(clooponline-tests
		test_agents/catch.hpp
		test_agents/ITester.h
		test_agents/ITester.cpp

		test_agents/identification/ValidIdentification.h
		test_agents/identification/NotIdentifiedRequest.h
		test_agents/identification/InvalidIdentification.h

		test_agents/manual_session/SessionCreation.h
		test_agents/manual_session/ValidJoin.h
		test_agents/manual_session/InvalidSIDJoin.h
		test_agents/manual_session/ValidPasswordJoin.h
		test_agents/manual_session/InvalidPasswordJoin.h
		test_agents/manual_session/JoinFalseAAJ.h
		test_agents/manual_session/JoinTrueAAJ.h

		test_agents/manager_selection/InitialManager.h
		test_agents/manager_selection/PermanentManagerSwitch.h
		test_agents/manager_selection/TempManagerSwitch.h

		test_agents/session_list/NoFilterSessionList.h
		test_agents/session_list/IncludedFilteredSessionList.h
		test_agents/session_list/ExcludedFilteredSessionList.h

		test_agents/leaving_session/PermanentLeave.h
		test_agents/leaving_session/TemporaryLeave.h
		test_agents/leaving_session/RejoinAfterTempLeave.h
		test_agents/leaving_session/LeaveAfterTemporaryLeave.h

		test_agents/rpc_call/SimpleRPCCall.h
		test_agents/rpc_call/RPCWithReceptors.h
		test_agents/rpc_call/RPCWithInvalidReceptors.h
		test_agents/rpc_call/RPCWithoutReceptors.h
		test_agents/rpc_call/RPCWithEmptyReceptors.h
		test_agents/rpc_call/ValidRPCAsOthers.h
		test_agents/rpc_call/InvalidRPCAsOthers.h

		test_agents/session_properties/SetValueProperty.h
		test_agents/session_properties/SetArrayProperty.h
		test_agents/session_properties/SetObjectProperty.h
		test_agents/session_properties/ModifyValueProperty.h
		test_agents/session_properties/ModifyArrayProperty.h
		test_agents/session_properties/ModifyObjectProperty.h
		test_agents/session_properties/ValidTryModify.h
		test_agents/session_properties/InvalidTryModify.h
		test_agents/session_properties/DeleteProperty.h
		test_agents/session_properties/MultiplePropertiesChange.h
		test_agents/session_properties/ChangePropertyType.h
		test_agents/session_properties/InvalidPathString.h
		test_agents/session_properties/InvalidArrayIndex.h
		test_agents/session_properties/IORArray.h

		test_agents/starting_session/ASBRAndAllReady.h
		test_agents/starting_session/ASBRAndAllNotReady.h
		test_agents/starting_session/FalseASBRAndAllReady.h
		test_agents/starting_session/FalseASBRAndNotAllReady.h
		test_agents/starting_session/StartAndSomeoneLeave.h
		test_agents/starting_session/StartAndManagerCancel.h
		test_agents/starting_session/CancelTruePCC.h
		test_agents/starting_session/CancelFalsePCC.h

		test_agents/disconnection/DCKick.h
		test_agents/disconnection/DCTerminate.h
		test_agents/disconnection/RejoinContinueAndKick.h
		test_agents/disconnection/LeaveContinueAndKick.h
		test_agents/disconnection/RejoinContinueAndTerminate.h
		test_agents/disconnection/LeaveContinueAndTerminate.h
		test_agents/disconnection/RejoinLockAndKick.h
		test_agents/disconnection/LeaveLockAndKick.h
		test_agents/disconnection/RejoinLockAndTerminate.h
		test_agents/disconnection/LeaveLockAndTerminate.h
		test_agents/disconnection/ManagerKick.h
		test_agents/disconnection/NotManagerKick.h
		test_agents/disconnection/ManagerTerminate.h
		test_agents/disconnection/NotManagerTerminate.h
		test_agents/disconnection/LockedSession.h

		test_agents/side/DeterminedAvailableSideJFS.h
		test_agents/side/DeterminedAvailableSideJAS.h
		test_agents/side/DeterminedAvailableSideJDS.h
		test_agents/side/NotDeterminedAvailableSideJAS.h
		test_agents/side/NotDeterminedAvailableSideJDS.h
		test_agents/side/ChangeSideBeforeStart.h
		test_agents/side/TrueCCSAS.h
		test_agents/side/FalseCCSAS.h
		test_agents/side/ManagerChangeAs.h
		test_agents/side/NotManagerChangeAs.h
		test_agents/side/NotManagerChangeCap.h
		test_agents/side/ManagerChangeCapNoKick.h
		test_agents/side/ManagerChangeCapMakeDC.h
		test_agents/side/ManagerChangeCapWithKick.h
		test_agents/side/ManagerChangeCapWithKickError.h

		test_agents/change_config_effect/ChangeCapacityEffect.h
		test_agents/change_config_effect/ChangeAAJEffect.h
		test_agents/change_config_effect/ChangeCCSASEffect.h
		test_agents/change_config_effect/ChangeDCBehaviorEffect.h
		test_agents/change_config_effect/ChangeDCTimeoutEffect.h
		test_agents/change_config_effect/ChangeOptionEffect.h
		test_agents/change_config_effect/ChangeLabelEffect.h
		test_agents/change_config_effect/ChangePasswordEffect.h

		test_agents/match_making/SubscribePackets.h
		test_agents/match_making/SuccessfulMatchMake.h
		test_agents/match_making/UnsuccessfulMatchMake.h
		test_agents/match_making/BestMatchMake.h
		test_agents/match_making/UnmatchedOptionsInPool.h
		test_agents/match_making/MMEqualIndicator.h
		test_agents/match_making/MMTimeoutVBetterMatch.h
		test_agents/match_making/ExistingSessionVBetterMatch.h
		)
configure_file("./configs/valid_configs.json" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}" COPYONLY)
target_link_libraries(clooponline-tests clooponline-testbot)

add_executable(clooponline-debug
		src/debug.cpp)
target_link_libraries(clooponline-debug clooponline-testbot)

add_executable(clooponline-benchmark
		benchmark/main.cpp
		benchmark/RPSBatchClient.h
		benchmark/RPSBatchClient.cpp)

target_link_libraries(clooponline-benchmark
		${clooponline-nethandler_LIBRARIES}
		cxxtools
		cxxtools-json)