#include "Bot.h"
#include <Client_Socket_TCP_Provider.h>
#include <Client_WS_TCP_Provider.h>
#include <Logger.h>
#include <ThriftParser.h>
#include <ThriftException.h>
#include <fstream>
#include <JsonParser.h>
#include <AAAChecker.h>
#include <ThriftBuffer.h>

using namespace std;
using namespace clooponline::packet;
extern Logger* loggerptr;

Bot::Bot()
{
   mAlwaysAcceptJoinFlag = false;
   mAllShouldBeReadyFlag = true;
   mPlayersCanCancelFlag = false;
   mCanChangeSideAfterStartFlag = false;
   mIsManager = false;
   mDisconnectionAction = DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT;
   mDisconnectionTimeout = 60;
   mStartTimeout = 5;
   mMinPlayersToMatch = 2;
   mSide = 0;
   mSessionPassword = "";
   mGameSideInfos[1] = 1;
}

void Bot::initialize(string configFileAddr, string targetObject)
{
   ifstream configFile(configFileAddr);
   string conf((istreambuf_iterator<char>(configFile)), istreambuf_iterator<char>());
   JsonParser doc;
   doc.deserialize(conf);
   JsonParser mp = (targetObject == "") ? std::move(doc) : doc.getObjectMember(targetObject.c_str());

   mServerIP = mp.getStringMember("server_ip");
   mGameServerPort = uint16_t(mp.getUintMember("game_server_port"));
   mAAAServerPort = uint16_t(mp.getUintMember("aaa_server_port"));
   mUID = mp.getUint64Member("UID");
   mPassword = mp.getStringMember("password");
   mAppToken = mp.getStringMember("app_token");
   string connstr = mp.getStringMember("conn_mode");

   if (connstr == "tcp_socket")
      mConnectionMode = CONNECTION_TCP_SOCKET;
   else if (connstr == "tcp_websocket")
      mConnectionMode = CONNECTION_TCP_WEBSOCKET;

   defineThreadResources<ThriftBuffer, ThriftParserFactory>(false);
   initialize();
}

void Bot::initialize()
{
   mAAAClient = new cxxtools::json::HttpClient(mServerIP, (unsigned short) mAAAServerPort, "/AAA/user");
   mAAARPCFunctions.generateAccount = new cxxtools::RemoteProcedure<GetIDResponse> (*mAAAClient, "generateAccount");
   mAAARPCFunctions.login = new cxxtools::RemoteProcedure<LoginResponse, uint64_t, string, string> (*mAAAClient, "login");
   mAAARPCFunctions.updateNickname = new cxxtools::RemoteProcedure<Response, uint64_t, string, string> (*mAAAClient, "updateNickname");
   mAAARPCFunctions.updateEmail = new cxxtools::RemoteProcedure<Response, uint64_t, string, string> (*mAAAClient, "updateEmail");
   mAAARPCFunctions.updateAvatar = new cxxtools::RemoteProcedure<Response, uint64_t, string, string> (*mAAAClient, "updateAvatar");
   mAAARPCFunctions.updatePassword = new cxxtools::RemoteProcedure<Response, uint64_t, string, string> (*mAAAClient, "updatePassword");
   mAAARPCFunctions.updateUser = new cxxtools::RemoteProcedure<Response, uint64_t, string, string, string, string, string> (*mAAAClient, "updateUser");
   mAAARPCFunctions.recover = new cxxtools::RemoteProcedure<Response, string> (*mAAAClient, "recover");
   mAAARPCFunctions.recoverWithToken = new cxxtools::RemoteProcedure<RocoverResponse, string, string> (*mAAAClient, "recoverWithToken");
   mAAARPCFunctions.getUserProfiles = new cxxtools::RemoteProcedure<UserInfoResponse, vector<uint64_t>> (*mAAAClient, "getUserProfiles");

   setTypeHandler(PacketType::BE_MANAGER, bind(&Bot::gotManager, this, _1, _2, _3));
   setTypeHandler(PacketType::RPC_CALLED, bind(&Bot::gotRPC, this, _1, _2, _3));
   setTypeHandler(PacketType::PROPERTIES_CHANGED, bind(&Bot::gotPropertiesChanged, this, _1, _2, _3));
   setTypeHandler(PacketType::SIDE_CHANGED, bind(&Bot::gotSideChanged, this, _1, _2, _3));
   setTypeHandler(PacketType::CONFIG_CHANGED, bind(&Bot::gotConfigChanged, this, _1, _2, _3));
   setTypeHandler(PacketType::LEFT, bind(&Bot::gotLeft, this, _1, _2, _3));
   setTypeHandler(PacketType::AVAILABLE_ACTIVE_SESSIONS, bind(&Bot::gotActiveSession, this, _1, _2, _3));
   setTypeHandler(PacketType::LOCKED, bind(&Bot::gotLock, this, _1, _2, _3));
   setTypeHandler(PacketType::RESUMED, bind(&Bot::gotResume, this, _1, _2, _3));
   setTypeHandler(PacketType::TERMINATED, bind(&Bot::gotTerminate, this, _1, _2, _3));
   setTypeHandler(PacketType::READY, bind(&Bot::gotReady, this, _1, _2, _3));
   setTypeHandler(PacketType::STARTED, bind(&Bot::gotStarted, this, _1, _2, _3));
   setTypeHandler(PacketType::JOINED, bind(&Bot::gotJoined, this, _1, _2, _3));
   setTypeHandler(PacketType::REJOINED, bind(&Bot::gotRejoined, this, _1, _2, _3));
   setTypeHandler(PacketType::START_CANCELED, bind(&Bot::gotStartCanceled, this, _1, _2, _3));
   setTypeHandler(PacketType::TEMP_LEFT, bind(&Bot::gotTempLeft, this, _1, _2, _3));
   setTypeHandler(PacketType::SUBSCRIPTION_RESULT, bind(&Bot::gotSubsResult, this, _1, _2, _3));
   setIdentifyHandler(bind(&Bot::connectedToServer, this, _1));
   setGeneralExceptionHandler(bind(&Bot::gotException, this, _1, _2, _3));

   switch (mConnectionMode)
   {
      case CONNECTION_TCP_SOCKET:
         addNetProvider<Client_Socket_TCP_Provider>(mServerIP, mGameServerPort);
         break;
      case CONNECTION_TCP_WEBSOCKET:
         addNetProvider<Client_WS_TCP_Provider>(mServerIP, mGameServerPort);
         break;
      default:
         break;
   }
}

Bot::~Bot()
{
   disconnect();
}

void Bot::login()
{
   LoginResponse re = (*(mAAARPCFunctions.login))(mUID, mPassword, mAppToken);
   if (re.result == "ok")
   {
      mOTP = re.OTP;
   }
   else if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
   {
      *loggerptr << "Authentication for \"" << mUID << "\"" << " was not successful!";
      loggerptr->flushLogBuffer();
   }
}

void Bot::connectToGameServer()
{
   Router::run<ThriftBuffer, ThriftParserFactory>(false);
   this_thread::sleep_for(chrono::milliseconds(500));
}

void Bot::connectedToServer(ProfilePtr server)
{
   mServer = server;
   auto iden = (ThriftParser*)createPacket();
   iden->setMessageType(PacketType::IDENTIFY);
   iden->pIdentify.__set_OTP(mOTP);
   sendPacket(server->CID, iden);
}

void Bot::gotManager(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(BeManager)
   mIsManager = true;
   mTypeCVs[PacketType::BE_MANAGER].notify_all();
}

void Bot::gotLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(Left)
   mCoPlayers.erase(uint(innerpack.UID));
}

void Bot::gotActiveSession(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(ActiveSessions)

   mAvailableActiveSessions.clear();
   for (auto sid : innerpack.sessionIDToConfigs)
      mAvailableActiveSessions.push_back(uint(sid.first));
}

void Bot::gotLock(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(Locked)
   mSessionState = GAME_STATE_PAUSED;
}

void Bot::gotResume(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(Resumed)
   mSessionState = GAME_STATE_PLAYING;
}

void Bot::gotReady(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(Ready)
   mSessionState = GAME_STATE_START_COUNTDOWN;
   sendReady();
}

void Bot::gotStarted(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(Started)

   for (auto um : innerpack.UIDToSideMap)
   {
      mCoPlayers[uint64_t(um.first)].Side = uint(um.second);
   }

   mSide = uint(innerpack.UIDToSideMap[mUID]);

   mTypeCVs[PacketType::STARTED].notify_all();
}

void Bot::gotTerminate(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(Terminated)
   mSessionState = gameState::GAME_STATE_TERMINATED;
}

void Bot::gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(Joined)
   mCoPlayers[uint64_t(innerpack.UID)].Side = uint(innerpack.side);
}

void Bot::gotRejoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(Rejoined)
}

void Bot::gotStartCanceled(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(StartCanceled)
   mSessionState = GAME_STATE_WAITING_FOR_START;
}

void Bot::gotTempLeft(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(TemporaryLeft)
}

void Bot::gotSubsResult(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(SubscriptionResult)
   mSessionID = uint(innerpack.sessionID);
}

void Bot::sendCreateSession()
{
   auto create = (ThriftParser*)createPacket();
   create->setMessageType(PacketType::CREATE_SESSION);
   fillSessionConfigs(create->pCreateSession.configs);
   create->pCreateSession.__set_side(mSide);
   sendRequest(mServer, create, bind(&Bot::createSessionDone, this, _1, _2, _3));
}

void Bot::createSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(CreateSessionResponse)
   mSessionID = uint(innerpack.sessionID);
   mTypeCVs[PacketType::CREATE_SESSION_RESPONSE].notify_all();
}

void Bot::sendJoinSession()
{
   auto join = (ThriftParser*)createPacket();
   join->setMessageType(PacketType::JOIN_SESSION);
   join->pJoinSession.__set_sessionID(mSessionID);
   join->pJoinSession.__set_side(mSide);
   join->pJoinSession.__set_password(mSessionPassword);
   sendRequest(mServer, join, bind(&Bot::joinSessionDone, this, _1, _2, _3), bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(JoinSessionResponse)
   mAlwaysAcceptJoinFlag = innerpack.configs.alwaysAcceptJoin;
   mCanChangeSideAfterStartFlag = innerpack.configs.canChangeSideAfterStart;
   mDisconnectionAction = innerpack.configs.disconnectionBehavior;
   mDisconnectionTimeout = uint(innerpack.configs.disconnectionTimeout);
   mSessionLabel = innerpack.configs.label;
   mLastRPCIndex = uint(innerpack.configs.lastRPCIndex);
   mGameOptions = innerpack.configs.options;
   mCoPlayers.clear();
   for (auto& ps :innerpack.configs.participantsToSide)
      mCoPlayers[uint64_t(ps.first)].Side = uint(ps.second);

   //TODO: innerpack.properties
   mPropertiesRevisionIndex = uint(innerpack.configs.propertyRevisionIndex);
   mGameSideInfos.clear();
   for (auto& sc : innerpack.configs.sideToCapacity)
      mGameSideInfos[uint(sc.first)] = uint(sc.second);

   mTypeCVs[PacketType::JOIN_SESSION_RESPONSE].notify_all();
}

void Bot::sendSessionList()
{
   auto slist = (ThriftParser*)createPacket();
   slist->setMessageType(PacketType::SESSION_LIST);
   slist->pSessionList.__set_filterOptions(mGameOptions);
   sendRequest(mServer, slist, bind(&Bot::sessionListDone, this, _1, _2, _3));
}

void Bot::sessionListDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(SessionListResponse)
   mSessionsList.clear();
   for (auto s : innerpack.IDtoConfigs)
   {
      mSessionsList[s.first] = s.second;
   }
}

void Bot::sendSubs(float indicator, uint poolTime, float maxIndicatorDistance)
{
   auto subs = (ThriftParser*)createPacket();
   subs->setMessageType(PacketType::SUBSCRIBE);
   subs->pSubscribe.__set_indicator(indicator);
   subs->pSubscribe.__set_poolTime(poolTime);
   subs->pSubscribe.__set_maxIndicatorDistance(maxIndicatorDistance);
   subs->pSubscribe.__set_side(mSide);
   fillSessionConfigs(subs->pSubscribe.configs);
   sendRequest(mServer, subs, bind(&Bot::subsDone, this, _1, _2, _3), bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::sendUSubs()
{
   auto usubs = (ThriftParser*)createPacket();
   usubs->setMessageType(PacketType::UNSUBSCRIBE);
   usubs->pUnsubscribe.__set_trackID(mSubscribeTID);
   sendRequest(mServer, usubs, 0, bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::sendTempLeave()
{
   auto tleave = (ThriftParser*)createPacket();
   tleave->setMessageType(PacketType::TEMP_LEAVE);
   tleave->pTemporaryLeave.__set_sessionID(mSessionID);
   sendPacket(mServer, tleave);
}

void Bot::sendHandleActiveSession(uint sid, string action, uint pindex, uint rpcindex)
{
   auto hasession = (ThriftParser*)createPacket();
   hasession->setMessageType(PacketType::HANDLE_ACTIVE_SESSION);
   hasession->pHandleActiveSession.__set_sessionID(sid);
   hasession->pHandleActiveSession.__set_action((action == "join") ? ActiveSessionActionType::JOIN : ActiveSessionActionType::LEAVE);
   hasession->pHandleActiveSession.__set_lastPropertiesRevisionIndex(pindex);
   hasession->pHandleActiveSession.__set_lastCachedRPCIndex(rpcindex);
   mSessionID = sid;
   sendRequest(mServer, hasession, bind(&Bot::handleActiveSessionDone, this, _1, _2, _3));
}

void Bot::sendReady()
{
   auto sready = (ThriftParser*)createPacket();
   sready->setMessageType(PacketType::SET_READY);
   sready->pSetReady.__set_sessionID(mSessionID);
   sendPacket(mServer, sready);
}

void Bot::sendProgress(std::string field, float progressDiff)
{
   static auto progressSender = new cxxtools::RemoteProcedure
                                <Response, uint64_t, std::string, std::string, float>(*mAAAClient, "updateProgress");
   (*progressSender)(mUID, mAppToken, field, progressDiff);
}

void Bot::sendAchievement(uint64_t achieved)
{
   static auto achievementSender = new cxxtools::RemoteProcedure
           <Response, uint64_t, uint64_t, std::string>(*mAAAClient, "achievementUnlocked");
   (*achievementSender)(mUID, achieved, mAppToken);
}

void Bot::sendStartSession()
{
   auto start = (ThriftParser*)createPacket();
   start->setMessageType(PacketType::START);
   start->pStart.__set_sessionID(mSessionID);
   start->pStart.__set_allShouldBeReadyFlag(mAllShouldBeReadyFlag);
   start->pStart.__set_playersCanCancelFlag(mPlayersCanCancelFlag);
   start->pStart.__set_timeout(mStartTimeout);
   sendPacket(mServer, start);
}

void Bot::sendCancelStartSession()
{
   auto cstart = (ThriftParser*)createPacket();
   cstart->setMessageType(PacketType::CANCEL_START);
   cstart->pCancelStart.__set_sessionID(mSessionID);
   sendRequest(mServer, cstart, 0, bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::sendSetProperty(std::string operation, std::string path, int val, int cachedVal)
{
   auto set = (ThriftParser*)createPacket();
   set->setMessageType(PacketType::SET_PROPERTIES);
   set->pSetProperties.__set_sessionID(mSessionID);

   PropertyInstance p;
   if (operation == "modify")
      p.__set_operation(OperationType::MODIFY);
   else if (operation == "try")
      p.__set_operation(OperationType::TRY_MODIFY);
   else if (operation == "delete")
      p.__set_operation(OperationType::DELETE);

   p.__set_path(path);
   p.__isset.value = true;
   p.value.type = ValueType::INT_32;
   p.value.__set_int32Val(val);
   p.__isset.cachedValue = true;
   p.cachedValue.type = ValueType::INT_32;
   p.cachedValue.__set_int32Val(cachedVal);

   set->pSetProperties.properties.push_back(p);
   sendRequest(mServer, set, bind(&Bot::setPropertiesDone, this, _1, _2, _3), bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::sendSetProperty(std::string operation, std::string path, std::string val)
{
   auto set = (ThriftParser*)createPacket();
   set->setMessageType(PacketType::SET_PROPERTIES);
   set->pSetProperties.__set_sessionID(mSessionID);

   PropertyInstance p;
   if (operation == "modify")
      p.__set_operation(OperationType::MODIFY);
   else if (operation == "try")
      p.__set_operation(OperationType::TRY_MODIFY);
   else if (operation == "delete")
      p.__set_operation(OperationType::DELETE);

   p.__set_path(path);
   p.__isset.value = true;
   p.value.type = ValueType::STRING;
   p.value.__set_strVal(val);
   set->pSetProperties.properties.push_back(p);
   sendRequest(mServer, set, bind(&Bot::setPropertiesDone, this, _1, _2, _3));
}

void Bot::sendSetProperty(std::string operation, std::string path, std::vector<int> val)
{
   auto set = (ThriftParser*)createPacket();
   set->setMessageType(PacketType::SET_PROPERTIES);
   set->pSetProperties.__set_sessionID(mSessionID);

   PropertyInstance p;
   if (operation == "modify")
      p.__set_operation(OperationType::MODIFY);
   else if (operation == "try")
      p.__set_operation(OperationType::TRY_MODIFY);
   else if (operation == "delete")
      p.__set_operation(OperationType::DELETE);

   p.__set_path(path);
   p.__isset.value = true;
   p.value.type = ValueType::ARRAY;
   p.value.__isset.array = true;
   for (auto v : val)
   {
      DOMNode d;
      d.__set_type(ValueType::INT_32);
      d.__set_int32Val(v);
      p.value.array.push_back(d);
   }

   set->pSetProperties.properties.push_back(p);
   sendRequest(mServer, set, bind(&Bot::setPropertiesDone, this, _1, _2, _3));
}

void Bot::sendSetProperty(std::string operation, std::string path, std::map<std::string, int> val)
{
   auto set = (ThriftParser*)createPacket();
   set->setMessageType(PacketType::SET_PROPERTIES);
   set->pSetProperties.__set_sessionID(mSessionID);

   PropertyInstance p;
   if (operation == "modify")
      p.__set_operation(OperationType::MODIFY);
   else if (operation == "try")
      p.__set_operation(OperationType::TRY_MODIFY);
   else if (operation == "delete")
      p.__set_operation(OperationType::DELETE);

   p.__set_path(path);
   p.__isset.value = true;
   p.value.type = ValueType::MAP;
   p.value.__isset.children = true;
   for (auto v : val)
   {
      DOMNode d;
      d.__set_type(ValueType::INT_32);
      d.__set_int32Val(v.second);
      p.value.children.insert(make_pair(v.first, d));
   }

   set->pSetProperties.properties.push_back(p);
   sendRequest(mServer, set, bind(&Bot::setPropertiesDone, this, _1, _2, _3));
}

void Bot::sendGetProperty()
{
   auto get = (ThriftParser*)createPacket();
   get->setMessageType(PacketType::GET_PROPERTIES);
   get->pGetProperties.__set_sessionID(mSessionID);
   sendRequest(mServer, get, bind(&Bot::getPropertiesDone, this, _1, _2, _3));
}

void Bot::sendRPC(uint64_t sendAs, bool cached)
{
   auto rpc = (ThriftParser*)createPacket();
   rpc->setMessageType(PacketType::RPC_CALL);
   rpc->pRPCCall.__set_sessionID(mSessionID);
   rpc->pRPCCall.__set_methodname("test");

   if (sendAs != 0)
      rpc->pRPCCall.__set_asUID(sendAs);

   if (cached)
      rpc->pRPCCall.__set_cached(true);

   DOMNode d;
   d.type = ValueType::INT_32;
   d.int32Val = 10;
   rpc->pRPCCall.parameters[0] = d;
   sendRequest(mServer, rpc, bind(&Bot::RPCCallDone, this, _1, _2, _3), bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::sendRPC(std::vector<uint64_t> receptors, uint64_t sendAs, bool cached)
{
   auto rpc = (ThriftParser*)createPacket();
   rpc->setMessageType(PacketType::RPC_CALL);
   rpc->pRPCCall.__set_sessionID(mSessionID);
   rpc->pRPCCall.__set_methodname("test");
   if (sendAs != 0)
   {
      rpc->pRPCCall.__set_asUID(sendAs);
   }
   if (cached)
   {
      rpc->pRPCCall.__set_cached(true);
   }
   DOMNode d;
   d.type = ValueType::INT_32;
   d.int32Val = 10;
   rpc->pRPCCall.parameters[0] = d;

   rpc->pRPCCall.__isset.receptors = true;
   for (auto r : receptors)
      rpc->pRPCCall.receptors.push_back(int64_t(r));
   sendRequest(mServer, rpc, bind(&Bot::RPCCallDone, this, _1, _2, _3), bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::sendChangeSide(uint newside, uint64_t asUID)
{
   auto cs = (ThriftParser*)createPacket();
   cs->setMessageType(PacketType::CHANGE_SIDE);
   cs->pChangeSide.__set_sessionID(mSessionID);
   cs->pChangeSide.__set_newSide(newside);
   if (asUID != 0) cs->pChangeSide.__set_asUID(asUID);
   sendRequest(mServer, cs, 0, bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::sendLeave()
{
   auto sl = (ThriftParser*)createPacket();
   sl->setMessageType(PacketType::LEAVE_SESSION);
   sl->pLeaveSession.__set_sessionID(mSessionID);
   sendPacket(mServer, sl);
}

void Bot::sendKick(uint64_t uid)
{
   auto sl = (ThriftParser*)createPacket();
   sl->setMessageType(PacketType::MANAGER_KICK);
   sl->pManagerKick.__set_sessionID(mSessionID);
   sl->pManagerKick.__set_UID(int64_t(uid));
   sendRequest(mServer, sl, 0, bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::sendTerminate()
{
   auto sl = (ThriftParser*)createPacket();
   sl->setMessageType(PacketType::TERMINATE);
   sl->pTerminate.__set_sessionID(mSessionID);
   sendRequest(mServer, sl, 0, bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::sendGetActiveSessions()
{
   auto sl = (ThriftParser*)createPacket();
   sl->setMessageType(PacketType::GET_AVAILABLE_ACTIVE_SESSIONS);
   sendRequest(mServer, sl, bind(&Bot::gotActiveSession, this, _1, _2, _3));
}

void Bot::sendChangeConfig(clooponline::packet::SessionConfigs conf)
{
   auto sl = (ThriftParser*)createPacket();
   sl->setMessageType(PacketType::CHANGE_CONFIGS);
   sl->pChangeConfigs.__set_sessionID(mSessionID);
   sl->pChangeConfigs.__set_configs(conf);
   sendRequest(mServer, sl, 0, bind(&Bot::gotException, this, _1, _2, _3));
}

void Bot::subsDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(SubscribeResponse)
   mSubscribeTID = uint(treqPacket->pSubscribeResponse.trackID);
}

void Bot::handleActiveSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(HandleActiveSessionResponse)
}

void Bot::RPCCallDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(RPCCallResponse)
   mLastRPCIndex = uint(innerpack.RPCindex);
}

void Bot::setPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(SetPropertiesResponse)
}

void Bot::getPropertiesDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(GetPropertiesResponse)
}

void Bot::disconnect()
{
   Router::shutdown();
}

void Bot::reconnect()
{
   clearNetProviders();
   switch (mConnectionMode)
   {
      case CONNECTION_TCP_SOCKET:
         addNetProvider<Client_Socket_TCP_Provider>(mServerIP, mGameServerPort);
         break;
      case CONNECTION_TCP_WEBSOCKET:
         addNetProvider<Client_WS_TCP_Provider>(mServerIP, mGameServerPort);
         break;
      default:
         break;
   }

   login();
   connectToGameServer();
}

void operator>>= (const cxxtools::SerializationInfo& si, Bot::Response& re)
{
   si.getMember("result") >>= re.result;
   si.getMember("reason") >>= re.reason;
}

void operator>>= (const cxxtools::SerializationInfo& si, Bot::GetIDResponse& re)
{
   si >>= (Bot::Response&)re;
   si.getMember("uid") >>= re.uid;
   si.getMember("pass") >>= re.pass;
}

void operator>>= (const cxxtools::SerializationInfo& si, Bot::LoginResponse& re)
{
   si >>= (Bot::Response&)re;
   si.getMember("OTP") >>= re.OTP;
}

void operator>>= (const cxxtools::SerializationInfo& si, Bot::RocoverResponse& re)
{
   si >>= (Bot::Response&)re;
   si.getMember("uid") >>= re.uid;
   si.getMember("pass") >>= re.pass;
}

void operator>>= (const cxxtools::SerializationInfo& si, Bot::UserInfo& re)
{
   si.getMember("uid") >>= re.uid;
   si.getMember("nickname") >>= re.nickname;
   si.getMember("avatar") >>= re.avatar;
}

void operator>>= (const cxxtools::SerializationInfo& si, Bot::UserInfoResponse& re)
{
   si >>= (Bot::Response&)re;
   const cxxtools::SerializationInfo& infos = si.getMember("infos");
   re.mInfos.reserve(infos.memberCount());
   for (cxxtools::SerializationInfo::ConstIterator it = infos.begin(); it != infos.end(); ++it)
   {
      re.mInfos.resize(re.mInfos.size() + 1);
      *it >>= re.mInfos.back();
   }
}

void Bot::gotPropertiesChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(PropertiesChanged)
}

void Bot::gotRPC(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(RPCCalled)
   mLastRPCIndex = uint(innerpack.RPCindex);
}

void Bot::gotSideChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(SideChanged)
}

void Bot::gotConfigChanged(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(ConfigsChanged)
}

void Bot::fillSessionConfigs(SessionConfigs& configs)
{
   configs.__isset.sideToCapacity = true;
   for (auto s : mGameSideInfos)
   {
      configs.sideToCapacity[int(s.first)] = int(s.second);
   }

   configs.__set_alwaysAcceptJoin(mAlwaysAcceptJoinFlag);
   configs.__set_canChangeSideAfterStart(mCanChangeSideAfterStartFlag);
   configs.__set_disconnectionBehavior(mDisconnectionAction);
   configs.__set_disconnectionTimeout(mDisconnectionTimeout);
   configs.__set_minPlayerToMatch(mMinPlayersToMatch);
   configs.__set_label(mSessionLabel);
   configs.__set_options(mGameOptions);
   configs.__set_password(mSessionPassword);
}

void Bot::gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(Exception)
}

void Bot::waitForPacket(clooponline::packet::PacketType::type packet)
{
   mutex m;
   unique_lock<mutex> l(m);
   mTypeCVs[packet].wait_for(l, std::chrono::seconds(10));
}
