#include <Bot.h>
#include <Logger.h>
#include <Singleton.h>
#include <ThriftBuffer.h>
#include <ThriftParser.h>

using namespace std;
shared_ptr<Logger> loggerptr = nullptr;

int main(int argc, char **argv)
{
   uint64_t uid = 1;
   string serverip = "localhost";
   string password = "botbot";
   string token = "b6f226d31f3934fe8d37c86ee52c6673da31fcee8451e143fadef892a485d8a6";
   map<string, string> options;
   map<uint, uint> sides;

   for (int i = 1; i < argc; ++i)
   {
      stringstream ss(argv[i]);
      string opt; ss >> opt;

      if (opt == "-user")
      {
         istringstream iss(argv[++i]);
         iss >> uid;
      }
      else if (opt == "-pass")
      {
         istringstream iss(argv[++i]);
         iss >> password;
      }
      else if (opt == "-sip")
      {
         istringstream iss(argv[++i]);
         iss >> serverip;
      }
      else if (opt == "-ds")
      {
         uint sideid = 0;
         uint snum = 0;
         {
            istringstream iss(argv[++i]);
            iss >> sideid;
         }
         {
            istringstream iss(argv[++i]);
            iss >> snum;
         }
         sides[sideid] = snum;
      }
      else if (opt == "-op")
      {
         string oname = "";
         string oval = "";
         {
            istringstream iss(argv[++i]);
            iss >> oname;
         }
         {
            istringstream iss(argv[++i]);
            iss >> oval;
         }
         options[oname] = oval;
      }
      else if (opt == "-at")
      {
         istringstream iss(argv[++i]);
         iss >> token;
      }
      else
      {
         std::cout << "Unknown command \"" << opt << "\". Command skipped.\n";
      }
   }

   loggerptr = Singleton<Logger>::construct("botlogs", LOG_SEVERITY_TRACE);
   loggerptr->run();

   Bot tb;
   tb.mServerIP = serverip;
   tb.mGameServerPort = 14442;
   tb.mAAAServerPort = 14448;
   tb.mUID = uid;
   tb.mPassword = password;
   tb.mAppToken = token;
   tb.mConnectionMode = CONNECTION_TCP_SOCKET;
   tb.mGameOptions = options;
   tb.mGameSideInfos = sides;
   tb.initialize();

   this_thread::sleep_for(chrono::milliseconds(500));

   tb.login();
   tb.connectToGameServer();

   this_thread::sleep_for(chrono::seconds(1));
   tb.defineThreadResources<ThriftBuffer, ThriftParserFactory>(false);
   while (true)
   {
      std::string command;
      std::cout << "\n>> ";
      std::getline(cin, command);
      if (command == "") continue;
      std::stringstream ss;
      ss << command;
      std::string cm;
      ss >> cm;

      if (cm == "dc")
      {
         tb.disconnect();
      }
      else if (cm == "reconnect")
      {
         tb.reconnect();
      }
      else if (cm == "subs")
      {
         tb.sendSubs();
      }
      else if (cm == "usubs")
      {
         tb.sendUSubs();
      }
      else if (cm == "rpc")
      {
         bool cached;
         ss >> cached;
         tb.sendRPC(0, cached);
      }
      else if (cm == "rpcas")
      {
         bool cached;
         uint64_t as;
         ss >> cached;
         ss >> as;
         tb.sendRPC(as, cached);
      }
      else if (cm == "start")
      {
         tb.sendStartSession();
      }
      else if (cm == "progress")
      {
         std::string field;
         float diff;
         ss >> field;
         ss >> diff;
         tb.sendProgress(field, diff);
      }
      else if (cm == "achievement")
      {
         uint64_t achieID;
         ss >> achieID;
         tb.sendAchievement(achieID);
      }
      else if (cm == "create")
      {
         tb.sendCreateSession();
      }
      else if (cm == "join")
      {
         uint sid;
         ss >> sid;
         tb.mSessionID = sid;
         tb.sendJoinSession();
      }
      else if (cm == "joinwp")
      {
         uint sid;
         string pass;
         ss >> sid;
         ss >> pass;
         tb.mSessionID = sid;
         tb.mSessionPassword = pass;
         tb.sendJoinSession();
      }
      else if (cm == "label")
      {
         std::string label;
         ss >> label;
         tb.mSessionLabel = label;
      }
      else if (cm == "leave")
      {
         tb.sendLeave();
      }
      else if (cm == "slist")
      {
         tb.sendSessionList();
      }
      else if (cm == "templeave")
      {
         tb.sendTempLeave();
      }
      else if (cm == "handleas")
      {
         uint sid;
         string action;
         uint pindex, rpcindex;
         ss >> sid;
         ss >> action;
         ss >> pindex;
         ss >> rpcindex;
         tb.sendHandleActiveSession(sid, action, pindex, rpcindex);
      }
      else if (cm == "aaj")
      {
         bool flag;
         ss >> flag;
         tb.mAlwaysAcceptJoinFlag = flag;
         cout << "set always accept flag to " << flag << endl;
      }
      else if (cm == "asr")
      {
         bool flag;
         ss >> flag;
         tb.mAllShouldBeReadyFlag = flag;
         cout << "set all should be ready flag to " << flag << endl;
      }
      else if (cm == "pcc")
      {
         bool flag;
         ss >> flag;
         tb.mPlayersCanCancelFlag = flag;
         cout << "set players can cancel flag to " << flag << endl;
      }
      else if (cm == "side")
      {
         uint newside;
         ss >> newside;
         tb.sendChangeSide(newside);
      }
      else if (cm == "property")
      {
         string operation;
         string path;
         int val;
         int cval;
         ss >> operation;
         ss >> path;
         ss >> val;
         ss >> cval;
         tb.sendSetProperty(operation, path, val, cval);
      }
      else if (cm == "gproperty")
      {
         tb.sendGetProperty();
      }
   }
}
