#include <Bot.h>
#include <Logger.h>
#include <Singleton.h>
#include <ThriftParser.h>
#include <ThriftException.h>

using namespace std;
using namespace clooponline::packet;
shared_ptr<Logger> loggerptr = nullptr;

class ValidJoin : public Bot
{
public:
   ValidJoin()
   {
      mGotJoined = false;
   }

   virtual void given()
   {
      Bot::initialize("valid_configs.json", "bot1");
      Bot::login();
      Bot::connectToGameServer();
      mGameSideInfos[1] = 1;
      mGameSideInfos[2] = 1;
      mGameOptions["a"] = "1";
      mGameOptions["b"] = "2";
      mAlwaysAcceptJoinFlag = mCanChangeSideAfterStartFlag = true;
      mDisconnectionAction = DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT;
      mDisconnectionTimeout = 10;
      mSessionLabel = "test_session";
      std::this_thread::sleep_for(std::chrono::seconds(1));
      std::this_thread::sleep_for(std::chrono::seconds(1));
   }

   virtual void when()
   {
      std::this_thread::sleep_for(std::chrono::seconds(1));
   }

   virtual void then()
   {
      std::this_thread::sleep_for(std::chrono::seconds(1));
   }

   virtual void tearDown()
   {
   }

   virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
   {
      Bot::gotJoined(server, reqPacket, repPacket);
      mGotJoined = true;
   }

   bool mGotJoined;
};

int main(int argc, char **argv)
{
   loggerptr = Singleton<Logger>::construct("", LOG_SEVERITY_TRACE);
   loggerptr->run();

   ValidJoin vj;
   vj.given();
   vj.when();
   vj.then();

   return 0;
}