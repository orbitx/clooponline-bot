#include <Client_Socket_TCP_Provider.h>
#include <AAAChecker.h>
#include "RPSBatchClient.h"
#include <ThriftException.h>
#include <Logger.h>
#include <ThriftBuffer.h>

using namespace std;
using namespace clooponline::packet;
extern Logger* loggerptr;

#define THROW_ERROR_CODE(ERROR_ENUM_NAME) \
      throw ThriftException(ExceptionCode::ERROR_ENUM_NAME);

#define HANDLE_METHOD_HEAD(METHOD) \
      auto treqPacket = (ThriftParser*)reqPacket; \
      auto trepPacket = (ThriftParser*)repPacket; \
      if (! treqPacket->__isset.p##METHOD) \
      { \
         THROW_ERROR_CODE(INVALID_PACKET) \
      } \
      METHOD& innerpack = treqPacket->p##METHOD;

std::atomic<uint> RPSBatchClient::SentRequests(0);
std::atomic<uint> RPSBatchClient::ReceivedResponses(0);
ThreadPool RPSBatchClient::CommonThreadPool(1);

RPSBatchClient::RPSBatchClient(ushort threadNum, std::string gsIP, ushort gsPort, std::string aaaIP, ushort aaaPort)
                        : Router(threadNum)
{
   mGameServerIP = std::move(gsIP);
   mGameServerPort = gsPort;
   mAAAIP = std::move(aaaIP);
   mAAAPort = aaaPort;
   mSendPacketType = MIX;

   mAAAClient = new cxxtools::json::HttpClient(mAAAIP, (unsigned short) mAAAPort, "/AAA/user");
   mAAARPCFunctions.generateAccount = new cxxtools::RemoteProcedure<GetIDResponse> (*mAAAClient, "generateAccount");
   mAAARPCFunctions.login = new cxxtools::RemoteProcedure<LoginResponse, uint64_t, string, string> (*mAAAClient, "login");
   mAAARPCFunctions.updateNickname = new cxxtools::RemoteProcedure<Response, uint64_t, string, string> (*mAAAClient, "updateNickname");
   mAAARPCFunctions.updateEmail = new cxxtools::RemoteProcedure<Response, uint64_t, string, string> (*mAAAClient, "updateEmail");
   mAAARPCFunctions.updateAvatar = new cxxtools::RemoteProcedure<Response, uint64_t, string, string> (*mAAAClient, "updateAvatar");
   mAAARPCFunctions.updatePassword = new cxxtools::RemoteProcedure<Response, uint64_t, string, string> (*mAAAClient, "updatePassword");
   mAAARPCFunctions.updateUser = new cxxtools::RemoteProcedure<Response, uint64_t, string, string, string, string, string> (*mAAAClient, "updateUser");
   mAAARPCFunctions.recover = new cxxtools::RemoteProcedure<Response, string> (*mAAAClient, "recover");
   mAAARPCFunctions.recoverWithToken = new cxxtools::RemoteProcedure<RocoverResponse, string, string> (*mAAAClient, "recoverWithToken");
   mAAARPCFunctions.getUserProfiles = new cxxtools::RemoteProcedure<UserInfoResponse, vector<uint64_t>> (*mAAAClient, "getUserProfiles");

   setTypeHandler(PacketType::BE_MANAGER, bind(&RPSBatchClient::gotManager, this, _1, _2, _3));
   setTypeHandler(PacketType::JOINED, bind(&RPSBatchClient::gotJoined, this, _1, _2, _3));
   setTypeHandler(PacketType::RPC_CALLED, bind(&RPSBatchClient::gotRPC, this, _1, _2, _3));
   setTypeHandler(PacketType::RPC_CALL_RESPONSE, bind(&RPSBatchClient::RPCCallDone, this, _1, _2, _3));

   setIdentifyHandler(bind(&RPSBatchClient::connectedToServer, this, _1));
   setGeneralExceptionHandler(bind(&RPSBatchClient::gotException, this, _1, _2, _3));
   Router::run<ThriftBuffer, ThriftParserFactory>(false);
}

void RPSBatchClient::connectBatchClients(uint clientNum, uint64_t startID, std::string password, std::string appToken)
{
   mClientNum = clientNum;
   mStartID = startID;
   mPassword = std::move(password);
   mAppToken = std::move(appToken);
   mConnectFinished = false;
   connectNextClient();
}

void RPSBatchClient::connectedToServer(ProfilePtr server)
{
   auto iden = (ThriftParser*)createPacket();
   iden->setMessageType(PacketType::IDENTIFY);
   iden->pIdentify.__set_OTP(mCachedOTP);
   sendPacket(server->CID, iden);
   uint64_t uid = mStartID + mUIDToServerID.size();
   mUIDToServerID.insert(make_pair(uid, server->CID));
   mServerIDToUID.insert(make_pair(server->CID, uid));
   connectNextClient();
}

void RPSBatchClient::gotManager(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
}

void RPSBatchClient::gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
}

void RPSBatchClient::gotRPC(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
}

void RPSBatchClient::gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
}

void RPSBatchClient::connectNextClient()
{
   if (mUIDToServerID.size() < mClientNum)
   {
      LoginResponse re = (*(mAAARPCFunctions.login))(mStartID + mUIDToServerID.size(), mPassword, mAppToken);
      if (re.result == "ok")
      {
         addNetProvider<Client_Socket_TCP_Provider>(mGameServerIP, mGameServerPort);
         mCachedOTP = re.OTP;
      }
      else
      {
         cout << "There was a problem connecting to AAA" << endl;
         exit(0);
      }
   }
   else
   {
      cout << "connected " << mClientNum << " clients to game server." << endl;
      mConnectFinished = true;
   }
}

void RPSBatchClient::joinClientsToSessions(uint sessionNum)
{
   while (!mConnectFinished)
      this_thread::sleep_for(chrono::milliseconds(1));

   uint pps = uint(ceil(float(mUIDToServerID.size()) / sessionNum));
   uint i = 0;
   for (auto& c : mUIDToServerID)
   {
      if (i % pps == 0)
      {
         mJustCreatedSessionID = 0;
         auto create = (ThriftParser*)createPacket();
         create->setMessageType(PacketType::CREATE_SESSION);
         create->pCreateSession.configs.__isset.sideToCapacity = true;
         create->pCreateSession.configs.sideToCapacity[1] = pps;
         sendRequest(getProfile(c.second), create, bind(&RPSBatchClient::createSessionDone, this, _1, _2, _3));
         while(mJustCreatedSessionID == 0)
            this_thread::sleep_for(chrono::milliseconds(1));
      }
      else
      {
         auto join = (ThriftParser*)createPacket();
         join->setMessageType(PacketType::JOIN_SESSION);
         join->pJoinSession.__set_sessionID(mJustCreatedSessionID);
         sendRequest(getProfile(c.second), join, bind(&RPSBatchClient::joinSessionDone, this, _1, _2, _3));
      }

      mUIDToSessionID[c.first] = mJustCreatedSessionID;
      ++i;
   }

   cout << "created and joined " << sessionNum << " sessions." << endl;
}

void RPSBatchClient::sendBatchRequest(uint rpspc, uint duration)
{
   uint rps = mUIDToServerID.size() * rpspc;
   auto start = chrono::steady_clock::now();
   auto meet = start;

   auto c = mUIDToServerID.begin();
   while (duration == 0 || chrono::duration_cast<chrono::seconds>(meet - start).count() < duration)
   {
      for (uint i = 0; i < rps; ++i)
      {
         auto rpc = (ThriftParser*)createPacket();
         rpc->setMessageType(PacketType::RPC_CALL);
         rpc->pRPCCall.__set_sessionID(mUIDToSessionID[c->first]);
         rpc->pRPCCall.__set_methodname("test");
         DOMNode d;
         d.type = ValueType::INT_32;
         d.int32Val = 10;
         rpc->pRPCCall.parameters[0] = d;
         sendPacket(getProfile(c->second), rpc);
         ++SentRequests;
         ++c; if (c == mUIDToServerID.end()) c = mUIDToServerID.begin();

         if (chrono::duration_cast<chrono::seconds>(chrono::steady_clock::now() - meet).count() >= 1)
         {
            cout << "." << endl;
            break;
         }
      }

      long elapsed = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - meet).count();
      if (elapsed < 1000)
         this_thread::sleep_for(chrono::milliseconds(1000 - elapsed));
      meet = chrono::steady_clock::now();
   }
}

void RPSBatchClient::secondIterated()
{
   uint sent = SentRequests.exchange(0);
   uint received = ReceivedResponses.exchange(0);
   cout << "sent: " << sent << ", received: " << received << endl;
   CommonThreadPool.AddEvent(&RPSBatchClient::secondIterated, 1);
}

void RPSBatchClient::RPCCallDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   ++ReceivedResponses;
}

void RPSBatchClient::createSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
   HANDLE_METHOD_HEAD(CreateSessionResponse)
   mJustCreatedSessionID = uint(innerpack.sessionID);
}

void RPSBatchClient::joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket)
{
}

void operator>>= (const cxxtools::SerializationInfo& si, RPSBatchClient::Response& re)
{
   si.getMember("result") >>= re.result;
   si.getMember("reason") >>= re.reason;
}

void operator>>= (const cxxtools::SerializationInfo& si, RPSBatchClient::GetIDResponse& re)
{
   si >>= (RPSBatchClient::Response&)re;
   si.getMember("uid") >>= re.uid;
   si.getMember("pass") >>= re.pass;
}

void operator>>= (const cxxtools::SerializationInfo& si, RPSBatchClient::LoginResponse& re)
{
   si >>= (RPSBatchClient::Response&)re;
   si.getMember("OTP") >>= re.OTP;
}

void operator>>= (const cxxtools::SerializationInfo& si, RPSBatchClient::RocoverResponse& re)
{
   si >>= (RPSBatchClient::Response&)re;
   si.getMember("uid") >>= re.uid;
   si.getMember("pass") >>= re.pass;
}

void operator>>= (const cxxtools::SerializationInfo& si, RPSBatchClient::UserInfo& re)
{
   si.getMember("uid") >>= re.uid;
   si.getMember("nickname") >>= re.nickname;
   si.getMember("avatar") >>= re.avatar;
}

void operator>>= (const cxxtools::SerializationInfo& si, RPSBatchClient::UserInfoResponse& re)
{
   si >>= (RPSBatchClient::Response&)re;
   const cxxtools::SerializationInfo& infos = si.getMember("infos");
   re.mInfos.reserve(infos.memberCount());
   for (cxxtools::SerializationInfo::ConstIterator it = infos.begin(); it != infos.end(); ++it)
   {
      re.mInfos.resize(re.mInfos.size() + 1);
      *it >>= re.mInfos.back();
   }
}