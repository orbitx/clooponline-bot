#include <Logger.h>
#include <Singleton.h>
#include "RPSBatchClient.h"

using namespace std;
shared_ptr<Logger> loggerptr = nullptr;

int main(int argc, char **argv)
{
   string token = "b6f226d31f3934fe8d37c86ee52c6673da31fcee8451e143fadef892a485d8a6";
   uint64_t startID = 100000003;
   std::string password = "123123";
   loggerptr = Singleton<Logger>::construct("", LOG_SEVERITY_NO_LOG);

   uint botnum = 100;
   RPSBatchClient::secondIterated();
   thread t(&ThreadPool::Run, &RPSBatchClient::CommonThreadPool);

   std::vector<RPSBatchClient*> batchers;
   while (botnum > 0)
   {
      uint cnum = (botnum > 99) ? 99 : botnum;
      botnum -= cnum;

      RPSBatchClient* rps = new RPSBatchClient(1, "localhost", 14442, "localhost", 14448);
      batchers.push_back(rps);
      rps->connectBatchClients(cnum, startID, password, token);
      startID += cnum;
      rps->joinClientsToSessions(uint(ceil(float(cnum) / 2)));
   }

   this_thread::sleep_for(chrono::seconds(1));
   std::vector<std::thread> threads;
   for (auto& b : batchers)
      threads.emplace_back(&RPSBatchClient::sendBatchRequest, b, 60, 0);

   for (auto& t : threads)
      t.join();
}


