#include <Router.h>
#include <ThriftParser.h>
#include <cxxtools/remoteprocedure.h>
#include <cxxtools/serializationinfo.h>
#include <cxxtools/json/httpclient.h>

class RPSBatchClient : public Router
{
   struct Response
   {
      std::string result;
      std::string reason;
   };

   struct GetIDResponse : public Response
   {
      uint64_t    uid;
      std::string pass;
   };

   struct LoginResponse : public Response
   {
      std::string OTP;
   };

   struct RocoverResponse : public Response
   {
      uint64_t    uid;
      std::string pass;
   };

   struct UserInfo
   {
      uint64_t    uid;
      std::string nickname;
      std::string avatar;
   };

   struct UserInfoResponse : public Response
   {
      std::vector<UserInfo> mInfos;
   };

   struct RPCFunctions
   {
      cxxtools::RemoteProcedure<GetIDResponse>* generateAccount;
      cxxtools::RemoteProcedure<LoginResponse, uint64_t, std::string, std::string>* login;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string>* updateNickname;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string>* updateEmail;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string>* updateAvatar;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string>* updatePassword;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string, std::string, std::string, std::string>* updateUser;
      cxxtools::RemoteProcedure<Response, std::string>* recover;
      cxxtools::RemoteProcedure<RocoverResponse, std::string, std::string>* recoverWithToken;
      cxxtools::RemoteProcedure<UserInfoResponse, std::vector<uint64_t>>* getUserProfiles;
      cxxtools::RemoteProcedure<UserInfoResponse, std::string, std::string>* getUserProfileFromEmail;
      cxxtools::RemoteProcedure<Response, uint64_t, std::string, std::string, float>* updateProgress;
      cxxtools::RemoteProcedure<Response, uint64_t, uint64_t, std::string>* achievementUnlocked;

      RPCFunctions()
      {
         generateAccount = nullptr;
         login = nullptr;
         updateNickname = nullptr;
         updateEmail = nullptr;
         updateAvatar = nullptr;
         updatePassword = nullptr;
         updateUser = nullptr;
         recover = nullptr;
         recoverWithToken = nullptr;
         getUserProfiles = nullptr;
         getUserProfileFromEmail = nullptr;
         updateProgress = nullptr;
         achievementUnlocked = nullptr;
      }

      ~RPCFunctions()
      {
         if (generateAccount)
            delete generateAccount;
         if (login)
            delete login;
         if (updateNickname)
            delete updateNickname;
         if (updateEmail)
            delete updateEmail;
         if (updateAvatar)
            delete updateAvatar;
         if (updatePassword)
            delete updatePassword;
         if (updateUser)
            delete updateUser;
         if (recover)
            delete recover;
         if (recoverWithToken)
            delete recoverWithToken;
         if (getUserProfiles)
            delete getUserProfiles;
         if (getUserProfileFromEmail)
            delete getUserProfileFromEmail;
         if (updateProgress)
            delete updateProgress;
         if (achievementUnlocked)
            delete achievementUnlocked;
      }
   };

   friend void operator>>= (const cxxtools::SerializationInfo& si, Response& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, GetIDResponse& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, LoginResponse& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, RocoverResponse& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, UserInfo& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, UserInfoResponse& re);

public:
   enum BatchPacketType
   {
      RPC = 0,
      PROPERTIES,
      MIX
   };

   RPSBatchClient(ushort threadNum, std::string gsIP, ushort gsPort, std::string aaaIP, ushort aaaPort);
   void connectBatchClients(uint clientNum, uint64_t startID, std::string password, std::string appToken);
   void joinClientsToSessions(uint sessionNum);
   void sendBatchRequest(uint rps, uint duration);
   static void secondIterated();
   static ThreadPool CommonThreadPool;

protected:

   cxxtools::json::HttpClient* mAAAClient;
   RPCFunctions  mAAARPCFunctions;

   static std::atomic<uint> SentRequests;
   static std::atomic<uint> ReceivedResponses;

   BatchPacketType mSendPacketType;
   bool mConnectFinished;
   std::map<uint, uint> mUIDToServerID;
   std::map<uint, uint> mServerIDToUID;
   std::map<uint, uint> mUIDToSessionID;
   uint mClientNum;
   uint64_t mStartID;
   std::string mPassword;
   std::string mGameServerIP;
   ushort mGameServerPort;
   std::string mAAAIP;
   ushort mAAAPort;
   std::string mAppToken;
   std::string mCachedOTP;
   uint mJustCreatedSessionID;

   void connectNextClient();
   virtual void connectedToServer(ProfilePtr server);
   virtual void gotManager(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotJoined(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotRPC(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void gotException(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void createSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void joinSessionDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);
   virtual void RPCCallDone(ProfilePtr server, PacketParser* reqPacket, PacketParser* repPacket);

};